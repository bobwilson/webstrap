import sys
import cPickle as pickle
import numpy

if len(sys.argv) != 2:
    exit()

doc_model_path = sys.argv[1]

ds = []
with open(doc_model_path, 'rb') as f:
    while True:
        try:
            ds.append(pickle.load(f))
        except:
            break

entities = set(e for d in ds for e in d.gold_links_by_position.itervalues())
mentions_with_links_per_doc = [len(d.gold_links_by_position) for d in ds]
mentions = [m for d in ds for m in d.mentions]
candidate_set_sizes = [len(m.candidates) for m in mentions]
candidate_recalled = [1.0 if d.gold_links_by_position[(m.begin, m.end)] in m.candidates else 0.0 for d in ds for m in d.mentions if (m.begin, m.end) in d.gold_links_by_position and d.gold_links_by_position[(m.begin, m.end)] != None]

print 'Docs:                        {:,d}'.format(len(ds))
print 'Linked Mentions:             {:,d}'.format(numpy.sum(mentions_with_links_per_doc))
print 'Total Mentions:              {:,d}'.format(len(mentions))
print 'Entities:                    {:,d}'.format(len(entities))

print 'KB Candidate Recall:         {:,.3f}'.format(numpy.mean(candidate_recalled))
print 'Avg Candidates per Mention:  {:,.2f}'.format(numpy.mean(candidate_set_sizes))
print 'Std Candidates per Mention:  {:,.2f}'.format(numpy.std(candidate_set_sizes))

doc_candidates = [sum(len(m.candidates) for m in d.mentions) for d in ds]
print 'Max Document Candidates:  {:,.2f}'.format(max(doc_candidates))
print 'Min Document Candidates:  {:,.2f}'.format(min(doc_candidates))
print 'Mean Document Candidates:  {:,.2f}'.format(numpy.mean(doc_candidates))
print 'Std Document Candidates:  {:,.2f}'.format(numpy.std(doc_candidates))
print ''
print 'Single Mention Docs: {:,.2f}'.format(sum(1.0 for d in ds if len(d.mentions) == 1))

#unlinkables = [(m,d.gold_links_by_position[(m.begin, m.end)]) for m in m.candidates for d in ds for m in d.mentions if d.gold_links_by_position[(m.begin, m.end)] not in m.candidates]#
#
#for m,gold in unlinkables:
#    print '## %s ## (%s)' % (m.text, gold)
#    for c in m.candidates:
#        print '\t%s' % c