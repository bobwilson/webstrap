import logging
import Queue
import multiprocessing

from collections import defaultdict
from bisect import bisect_left, bisect_right

log = logging.getLogger()

def group(iteration, key_getter, value_getter):
    d = defaultdict(list)
    for item in iteration:
        d[key_getter(item)].append(value_getter(item))
    return d

def invert_grouping(g):
    d = defaultdict(list)
    for k, items in g.iteritems():
        for i in items:
            d[i].append(k)
    return d

def spanset_insert(indicies, begin, end):
    """ Determines if a span from an index set is occupied in O(log(n)) """

    b_idx = bisect_right(indicies, begin)
    e_idx = bisect_left(indicies, end)

    can_insert = b_idx == e_idx and \
                 (b_idx == 0 or indicies[b_idx - 1] != begin) and \
                 (e_idx == len(indicies) or indicies[e_idx] != end) and \
                 b_idx % 2 == 0

    if can_insert:
        indicies.insert(b_idx, begin)
        indicies.insert(b_idx + 1, end)

    return can_insert

def spawn_worker(f):
    def fun(wid, q_in, q_out, recycle_interval):
        job_count = 0
        while True:
            i,x = q_in.get()
            if i is None:
                break
            try:
                recycle_id = wid if job_count + 1 == recycle_interval else None

                q_out.put(((i, f(x)), recycle_id))
                job_count += 1

                if recycle_id != None:
                    return
            except Exception as e:
                log.error("Worker function exception: %s" % e)
                raise
    return fun

def iter_to_input_queue(iteration, q_in, p_control):
    iteration_len = 0
    for i, x in enumerate(iteration):
        q_in.put((i, x))
        iteration_len += 1

    p_control.send(iteration_len)
    p_control.close()

class parmapper(object):
    def __init__(self, job, nprocs = 8, recycle_interval = 5):
        self.job = job
        self.q_in = multiprocessing.Queue(1)
        self.q_out = multiprocessing.Queue(nprocs)
        self.recycle_interval = recycle_interval
        self.procs = [self.get_process(i) for i in range(nprocs)]

    def get_process(self, idx):
        return multiprocessing.Process(
            target=spawn_worker(self.job),
            args=(idx, self.q_in, self.q_out, self.recycle_interval))

    def run_process(self, idx):
        self.procs[idx].daemon = True
        self.procs[idx].start()

    def __enter__(self):
        for i in xrange(len(self.procs)):
            self.run_process(i)
        return self

    def recycle_worker(self, wid):
        worker = self.procs[wid]
        #log.debug('Recycling worker id=%i, pid=%i...' % (wid, worker.pid))
        worker.join()
        self.procs[wid] = self.get_process(wid)
        self.run_process(wid)

    def consume(self, producer):
        worker_pipe, control_pipe = multiprocessing.Pipe(True)
        async_input_iterator = multiprocessing.Process(target=iter_to_input_queue,args=(producer, self.q_in, worker_pipe))
        async_input_iterator.daemon = True
        async_input_iterator.start()

        expected_output_count = None
        output_count = 0

        while expected_output_count == None or expected_output_count > output_count:
            if expected_output_count == None and control_pipe.poll():
                expected_output_count = control_pipe.recv()
                log.debug(
                    'Producer exhausted with %i items total, %i remaining...' % 
                    (expected_output_count, expected_output_count - output_count))
            try:
                # couldn't get this working without a busy wait
                out, recycle_wid = self.q_out.get_nowait()
                while True:
                    if recycle_wid != None:
                        self.recycle_worker(recycle_wid)
                    yield out
                    output_count += 1
                    out, recycle_wid = self.q_out.get_nowait()
            except Queue.Empty: pass

        async_input_iterator.join()

    def __exit__(self, t, value, traceback):
        for _ in self.procs:
            self.q_in.put((None,None))
        for p in self.procs:
            p.join() # todo: kill after some timeout
