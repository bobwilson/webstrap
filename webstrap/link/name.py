#!/usr/bin/env python
from linkers.link import Link
from processors.tok import WhitespaceTokeniser
from processors.tag import LookupTagger
from processors.dab import EntityDisambiguator
from processors.dab import NameDisambiguator
from processors.dab import Interpolator
from processors.nil import NilThresholder

NIL_THRESHOLD = 0.5

class Name(Link):
    def __init__(self, thresh=NIL_THRESHOLD):
        self.processors = [
            WhitespaceTokeniser()
            LookupTagger(),
            Interpolator([
                    (0.5, NameDisambiguator()),
                    (0.5, EntityDisambiguator()),
                    ]),
            NilThresholder(thresh),
            ]
