#!/usr/bin/env python
from ..process.rank import DummyRanker
from ..process.resolve import ThresholdResolver
from ..process.tag import DummyTagger
from ..process.tokenise import NON_WHITESPACE_RE
from ..process.tokenise import RegexTokeniser
from .link import Link

NIL_THRESHOLD = 0.5

class Dummy(Link):
    """Dummy linker for testing."""
    def __init__(self):
        self.processors = [
            RegexTokeniser(regex=NON_WHITESPACE_RE),
            DummyTagger(),
            DummyRanker(),
            ThresholdResolver(NIL_THRESHOLD),
            ]
