#!/usr/bin/env python
from .link import Link
from ..process.rank import EntityRanker
from ..process.resolve import ThresholdResolver
from ..process.tag import LookupTagger
from ..process.tokenise import NON_WHITESPACE_RE
from ..process.tokenise import RegexTokeniser
import os

NIL_THRESHOLD = 0.5
MODEL_DIR = '/data0/linking/erd/dev/models/'

class ErdShortEntity(Link):
    def __init__(self):
        self.processors = [
            RegexTokeniser(NON_WHITESPACE_RE),
            LookupTagger(MODEL_DIR, lower=True),
            EntityRanker(MODEL_DIR),
            ThresholdResolver(NIL_THRESHOLD),
            GreedyOverlapResolver()
            ]
