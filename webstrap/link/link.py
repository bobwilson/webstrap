#!/usr/bin/env python
LINKER_SET = set()

class Link(object):
    """A linker is a pipeline of processors."""
    @staticmethod
    def Register(c):
        LINKER_SET.add(c)
        return c

    def __init__(self):
        """self.processors should be a list of processor objects."""
        raise NotImplementedError

    def __call__(self, doc):
        for p in self.processors:
            doc = p(doc)
        return doc
