#!/usr/bin/env python
from .link import Link
from ..process.rank import HitCountRanker,NameProbabilityRanker,MultiplicativeRanker
from ..process.resolve import ThresholdResolver,GreedyOverlapResolver,ResolutionEntityMapper
from ..process.tag import ExactMatchTagger
from ..process.tokenise import NON_WHITESPACE_RE
from ..process.tokenise import RegexTokeniser

from ..model.model import HitCount,ErdEntity

from collections import defaultdict

import logging

log = logging.getLogger()

class ErdShortHitCountBaseline(Link):
    def __init__(self):
        model_path = '/data0/linking/erd/full/models/'

        hitcounts = HitCount()
        hitcounts.read(model_path)

        erdentities = ErdEntity()
        erdentities.read(model_path)

        wkid_to_fbid = dict()
        entities_by_name = defaultdict(list)
        for wkid, fbid, name in erdentities.iteritems():
            wkid_to_fbid[wkid] = fbid
            entities_by_name[name].append(wkid)

        self.processors = [
            RegexTokeniser(NON_WHITESPACE_RE),
            ExactMatchTagger(entities_by_name),
            HitCountRanker(hitcounts),
            ThresholdResolver(0.42), # (>= X% of largest order of magnitude)
            GreedyOverlapResolver(),
            ResolutionEntityMapper(lambda e: wkid_to_fbid.get(e, e))
        ]