#!/usr/bin/env python

from .link import Link
from ..process.rank import Ranker
from ..process.resolve import ThresholdResolver, GreedyOverlapResolver
from ..process.tag import GoldTagger
from ..process.tokenise import RegexTokeniser, NON_WHITESPACE_RE
from ..harness.harness import Harness, BatchLinkingHarness

from webstrap.model.model import EntityOccurrence

import numpy
import math
import random
import marshal
import cPickle as pickle
import operator
import logging

log = logging.getLogger()

@BatchLinkingHarness.Register
class DiscriminativeLinker(Link):
    """ Un-supervised linker taking pre-computed features """
    def __init__(self, feature):

        log.debug('Loading %i features...' % len(feature))
        feature_set = [pickle.load(open(f, 'rb')) for f in feature]

        log.debug('Initialising linker pipeline...')

        self.processors = [
            DiscriminativeRanker(feature_set),
            ThresholdResolver(None),
            GreedyOverlapResolver()
        ]

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('--feature', metavar='FEATURE_MODEL_PATHS', action='append')
        p.set_defaults(linkcls=cls)
        return p

class DiscriminativeRanker(Ranker):
    """ see: Graph Ranking for Collective Named Entity Disambiguation ACL2014 """
    def __init__(self, doc_features):
        self.doc_features = doc_features

    def get_feature_vector(self, doc, mention, candidate):
        return numpy.array([f[doc.id][(mention.begin, mention.end)][candidate] for f in self.doc_features])
    
    def _score(self, mention, doc):
        max_diff = 0
        max_i = 0
        
        candidate_features = [(c, self.get_feature_vector(doc, mention, c)) for c in mention.candidates]
        candidate_feature_sums = sorted([(numpy.sum(fv), c) for c, fv in candidate_features], reverse=True)
        candidate_feature_products = sorted([(numpy.prod(fv), c) for c, fv in candidate_features], reverse=True)

        candidate_scores = candidate_feature_sums 
        if len(candidate_features) >= 2:
            sums_diff = candidate_feature_sums[0][0] - candidate_feature_sums[1][0]
            prod_diff = candidate_feature_products[0][0] - candidate_feature_products[1][0]
            if prod_diff > sums_diff:
                candidate_scores = candidate_feature_products

        return candidate_scores