#!/usr/bin/env python

from .link import Link
from ..process.rank import Ranker
from ..process.resolve import ThresholdResolver, GreedyOverlapResolver
from webstrap.feature.feature import FEATURE_SET

import json
import numpy
import math
import cPickle as pickle
import logging

log = logging.getLogger()

def zero_mean_unit_variance_mapping(fv, params):
    mean, std = params
    return (fv - mean) / std

def polynomial_feature_mapping(feature_values):
    fv = [math.sqrt(2.0) * v for v in feature_values]

    sz = len(fv)
    for i in xrange(0, sz):
        for j in xrange(i, sz):
            weight = 1.0 if i != j else math.sqrt(2.0)
            fv.append(weight * fv[i]*fv[j])

    fv.append(1.0)

    return numpy.array(fv)

@Link.Register
class SupervisedLinker(Link):
    """ Linker making use of a supervised ranker. """
    def __init__(self, classifier_model_path, tagger):
        log.info('Initialising supervised linker...')
        classifier = pickle.load(open(classifier_model_path, 'rb'))

        log.debug('Loaded classifier with %i weights...', len(classifier.coef_.T))

        from ..process.tokenise import RegexTokeniser

        self.processors = [
            RegexTokeniser(),
            tagger,
            SupervisedRanker(classifier, polynomial_feature_mapping, self.feature_extraction),
            ThresholdResolver(None),
            GreedyOverlapResolver()
        ]

    def feature_extraction(self, doc, mention, candidate):
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('classifier_model_path', metavar='CLASSIFIER_MODEL')
        p.set_defaults(linkcls=cls)
        return p

@Link.Register
class OnlineSupervisedLinker(SupervisedLinker):
    """ Supervised linker computing document features on demand """
    def __init__(self, config_path):
        log.info('Loading feature extraction configuration: %s ...', config_path)

        #self.config = json.load(open(config_path,'r'))
        self.config = json.loads("""{
            "tagger": {
                "name": "StanfordTagger",
                "params": {
                    "tools_path": "/data0/linking/ner/stanford-ner-2014-08-27/",
                    "name_model_path": "/data0/linking/models/wikipedia.name.model"
                }
            },
            "features": [{
                    "name": "EntityProbability",
                    "params": {
                        "entity_model_path":    "/data0/linking/models/wikipedia.entity.model"
                    },
                    "std_model_path": "/data0/linking/data/conll/features/train.EntityProbability.wikipedia.feature.model.standardisation.model"
                }, {
                    "name":"NameProbability",
                    "params": {
                        "name_model_path":      "/data0/linking/models/wikipedia.name.model"
                    },
                    "std_model_path": "/data0/linking/data/conll/features/train.NameProbability.wikipedia.feature.model.standardisation.model"
                }
            ],
            "ranker": {
                "classifier_model_path": "/data0/linking/data/conll/features/classifiers/1255f4803419e2e273100b441fb69115.classifier.model"
            }
        }""")

        from ..process.tag import StanfordTagger

        super(OnlineSupervisedLinker, self).__init__(
            self.config['ranker']['classifier_model_path'],
            StanfordTagger(**self.config['tagger']['params']))

        extractors = {f.__name__:f for f in FEATURE_SET}

        invalid_features = [f['name'] for f in self.config['features'] if f['name'] not in extractors]
        if invalid_features:
            raise Exception("Unknown features: (%s)" % ', '.join(invalid_features))

        self.feature_extractors = [extractors[f['name']](**f['params']) for f in self.config['features']]

        log.info('Loading standardisation parameters...')
        std_params = [pickle.load(open(f['std_model_path'],'rb')) for f in self.config['features']]
        self.feature_mean, self.feature_std = zip(*std_params)

    def standardise(self, fv):
        return (fv - self.feature_mean) / self.feature_std

    def feature_extraction(self, doc, mention, candidate):
        fv = numpy.array([f.compute(doc, mention, candidate) for f in self.feature_extractors])
        return self.standardise(fv)

    @classmethod
    def add_arguments(cls, p):
        #super(OnlineSupervisedLinker, cls).add_arguments(p)
        p.add_argument('config_path', metavar='FEATURE_EXTRACTOR_CONFIG_PATH')
        p.set_defaults(linkcls=cls)
        return p

class OfflineSupervisedLinker(SupervisedLinker):
    """ Supervised linker taking pre-computed features """

    def __init__(self, classifier_model_path, feature):
        super(OfflineSupervisedLinker, self).__init__(classifier_model_path)

        log.info('Loading pre-computed features values...')
        self.doc_feature_values = [pickle.load(open(f, 'rb')) for f in feature]

    def feature_extraction(self, doc, mention, candidate):
        return numpy.array(f[doc.id][(mention.begin, mention.end)][candidate] for f in self.doc_feature_values)

    @classmethod
    def add_arguments(cls, p):
        super(OfflineSupervisedLinker, cls).add_arguments(p)
        p.add_argument('--feature', metavar='FEATURE_MODEL_PATHS', action='append')
        return p

class SupervisedRanker(Ranker):
    """ Ranks candidates using a classifier """
    def __init__(self, classifier, feature_mapper, feature_extraction):
        self.classifier = classifier
        self.feature_mapper = feature_mapper
        self.feature_extraction = feature_extraction

    def get_candidate_score(self, doc, mention, candidate):
        fv = self.feature_extraction(doc, mention, candidate)
        fv = self.feature_mapper(fv)

        return numpy.dot(fv.T, self.classifier.coef_.T)

    def _score(self, mention, doc):
        for c in mention.candidates:
            yield self.get_candidate_score(doc, mention, c), c