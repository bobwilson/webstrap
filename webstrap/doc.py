#!/usr/bin/env python

class Doc(object):
    def __init__(self, text, id=None, gold_links=None):
        self.text = text
        self.id = id
        self.chains = []

        if gold_links != None:
            self.gold_links_by_position = {p:e for e, p in gold_links}

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return u'Doc<%s, "%s">' % (self.id, self.text)

class MentionChain(object):
    """ Chain of coreferential mentions. """
    def __init__(self, mentions):
        self.mentions = mentions

        # chains are sorted
        self.begin = mentions[0].begin
        self.end = mentions[-1].end

        # handy backwards reference
        for m in mentions:
            m.chain = self

class Annotation(object):
    def __init__(self, begin, text, candidates=None, resolution=None):
        self.begin = begin
        self.text = text
        self.candidates = candidates # list of (score, entity) tuples
        self.resolution = resolution # (score, entity) tuple

    def __str__(self):
        return 'Annot<%d, "%s">' % (self.begin, self.text)

    @property
    def end(self):
        return self.begin + len(self.text)

    def copy(self):
        clone = Annotation(self.begin,
                           self.text,
                           self.candidates,
                           self.resolution)

        for k in self.__dict__.iterkeys():
            if k not in clone.__dict__:
                clone.__dict__[k] = self.__dict__[k]
        
        return clone
