#!/usr/bin/env python
import argparse
import re
import sys
import textwrap

from .harness.erd_short import ErdShort, ErdShortHost, ErdShortEval
from .harness.conll import Conll,ConllPrepare
from .harness.harness import ServiceHarness
from .process.train import Train
from .process.tag import CoreferenceTagger
from .harness.tac import Tac,TacPrepare
from .feature.extract import ExtractFeature
from .model.build.dbpedia import BuildDbpediaLinks, BuildDbpediaLexicalisations
from .model.build.dbpedia import BuildDbpediaRedirects
from .model.build.wikipedia import BuildWikipediaDocrep, BuildWikipediaRedirects
from .model.build.wikilinks import BuildWikilinksLexicalisations
from .model.build.wikilinks import BuildWikilinksMentions
from .model.build.wikilinks import BuildWikilinksEntityContext
from .model.build.derived import BuildOccurrenceFromMentions
from .model.build.derived import BuildOccurrenceFromLinks
from .model.build.derived import BuildEntityCooccurrenceFromOccurrence
from .model.build.derived import BuildLinkModels, BuildEntitySet
from .model.build.kopi import BuildKopiWikiEntityContext
from .model.build.wikipedia import BuildWikiHitCounts
from .model.build.wordvec import BuildWordVectors
from .model.build.yago import BuildYagoMeansNames
from .model.build.derived import BuildTitleRedirectNames
from .model.build.derived import BuildAliasFromNameModel
from .model.build.freebase import BuildFreebaseCandidates
from .model.build.derived import BuildIdfsForEntityContext, FilterTermModel
import logging

""" Logging Configuration """
logFormat = '%(asctime)s|%(levelname)s|%(module)s|%(message)s'
logging.basicConfig(format=logFormat)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

APPS = [
    ServiceHarness,
    BuildLinkModels,

    BuildDbpediaLexicalisations,
    BuildDbpediaLinks,
    BuildDbpediaRedirects,
    BuildWikipediaRedirects,
    BuildWikilinksLexicalisations,
    BuildWikiHitCounts,
    BuildKopiWikiEntityContext,
    BuildWikilinksMentions,
    BuildOccurrenceFromMentions,
    BuildOccurrenceFromLinks,
    BuildEntityCooccurrenceFromOccurrence,
    BuildWikilinksEntityContext,
    BuildWordVectors,
    BuildYagoMeansNames,
    BuildTitleRedirectNames,
    BuildAliasFromNameModel,
    BuildFreebaseCandidates,
    BuildIdfsForEntityContext,
    FilterTermModel,
    BuildWikipediaDocrep,

    BuildEntitySet,
    CoreferenceTagger,

    ConllPrepare,
    ExtractFeature,
    Train,

    TacPrepare,

    Tac,
    Conll,
    
    ErdShort,
    ErdShortHost,
    ErdShortEval
]

def main(args=sys.argv[1:]):
    p = argparse.ArgumentParser(description='Linkers built from web links.')
    sp = p.add_subparsers()
    for cls in APPS:
        name = re.sub('([A-Z])', r'-\1', cls.__name__).lstrip('-').lower()
        help = cls.__doc__.split('\n')[0]
        desc = textwrap.dedent(cls.__doc__.rstrip())
        csp = sp.add_parser(name,
                            help=help,
                            description=desc,
                            formatter_class=argparse.RawDescriptionHelpFormatter)
        cls.add_arguments(csp)
    namespace = vars(p.parse_args(args))
    cls = namespace.pop('cls')
    try:
        obj = cls(**namespace)
    except ValueError as e:
        p.error(str(e))
    obj()

if __name__ == '__main__':
    main()
