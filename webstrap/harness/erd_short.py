#!/usr/bin/env python
import re
from .harness import Harness, ServerHarness
from .eval import Evaluation
from ..doc import Doc
from ..util import group
from collections import defaultdict

def format_links(doc):
    """
    Write ERD short text format, e.g.:
    TREC-7    0    /m/04cnvy    bowflex            1
    TREC-8    0    /m/03d452    brooks brothers    1
    """
    links = []
    for l in doc.links:
        score, entity = l.resolution
        line = [
            doc.id, # query id
            str(0), # interpretation set
            entity, # entity id
            l.text, # mention text
            str(score), # score
            ]
        links.append('\t'.join(line))

    return '\n'.join(links)

def iter_annotations(f):
    for l in f:
        parts = l.split('\t')
        if len(parts) == 5:
            yield tuple(parts)

class ErdShortEval(Evaluation):
    "ERD Short Task Evaluation"
    def group_annotation_sets(self, annots_iter):
        annots = defaultdict(lambda: defaultdict(set))
        
        for query, intp_id, entity, mention, conf in annots_iter:
            annots[query][intp_id].add(entity)

        for query in annots.keys():
            for intp in annots[query].keys():
                annots[query][intp] = frozenset(annots[query][intp])

        return annots

    def eval(self, f_input, f_labels):
        results = list()
        inpt = self.group_annotation_sets(iter_annotations(f_input))
        gold = self.group_annotation_sets(iter_annotations(f_labels))

        for query in gold.keys():
            i_intps = set(inpt[query].values())
            g_intps = set(gold[query].values())
            
            matching_intps = g_intps.intersection(i_intps)
            matches = len(matching_intps)

            p = 0.0 if matches == 0 else matches / float(len(i_intps))
            r = 0.0 if matches == 0 else matches / float(len(g_intps))
            f = 0.0 if p == 0 or r == 0 else 2 * p * r / (p + r)
            
            results.append((query, f, p, r))
            
        return results

    def format(self, results):
        """Format results for printing"""
        t_f, t_p, t_r = 0.0, 0.0, 0.0

        for q, f, p, r in results:
            t_f += f
            t_p += p
            t_r += r

        return "F = %0.4f, p = %0.4f, r = %0.4f" % (t_f / len(results), t_p / len(results), t_r / len(results))

class ErdShort(Harness):
    "Harness for linking SIGIR14 ERD Workshop short text data files."
    def read(self, f):
        """
        Read ERD short text format, e.g.:
        TREC-6    blue throated hummingbird
        TREC-7    bowflex power pro
        TREC-8    brooks brothers clearance
        """
        with open(f, 'rd') as f:
            for line in f:
                if line.rstrip() == '':
                    continue # ignore empty lines
                id, text = line.rstrip().split('\t')
                yield Doc(text, id)

    def format(self, doc):
        return format_links(doc)

class ErdShortHost(ServerHarness):
    "Harness for hosting a SIGIR14 ERD Workshop short query linker."
    def read(self, params):
        """
        Read ERD POST data input, e.g.:
        dict ({
            runID:  'baseline.short'
            TextID: 'TREC-6'
            Text:   'blue throated hummingbird'
        })
        """
        text_param  = 'Text'
        id_param = 'TextID'

        if text_param in params and id_param in params:
            text, id = params[text_param], params[id_param]
            if text != '' and id != '':
                yield Doc(text, id)

    def format(self, doc):
        return format_links(doc)