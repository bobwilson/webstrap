#!/usr/bin/env python
import sys
import logging
import argparse
import textwrap
import cPickle as pickle
from .http import HttpServerRequestHandler
from ..util import parmapper
from ..doc import Doc
import json
from flask import Flask, request, Response

# not referenced explicitly but must be imported to register linkers
from webstrap.link import supervised # pylint: disable=I0011,W0611
from webstrap.link.link import LINKER_SET

# not referenced explicitly but must be imported to register extractable features
from webstrap.feature import probability, context, meta, freebase # pylint: disable=I0011,W0611

log = logging.getLogger()

class Harness(object):
    def __init__(self, **kwargs):
        self.in_path = kwargs.pop('fname')
        self.out_fh = sys.stdout if 'out_fh' not in kwargs else kwargs.pop('out_fh')
        self.link = kwargs.pop('linkcls')(**kwargs)

    def __call__(self):
        """Link documents """

        with parmapper(self.link, 30, recycle_interval=1) as pm:
            log.debug("Starting parallel link...")
            total = 0
            for i, doc in pm.consume(self.read(self.in_path)):
                total += 1
                if total % 50 == 0: log.debug('Linked %i docs...' % total)
                #log.debug('Linked doc #%i: %s' % (i, doc.id))
                out = self.format(doc).encode('utf-8')
                if out:
                    print >>self.out_fh, out

        log.debug('Finished linking documents')

    def read(self, f):
        """Yield Doc objects from path f"""
        raise NotImplementedError

    def format(self, doc):
        """Format Doc object for printing"""
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('fname', metavar='FILE')
        p.set_defaults(cls=cls)

        sp = p.add_subparsers()
        for linkcls in LINKER_SET:
            name = linkcls.__name__
            help_str = linkcls.__doc__.split('\n')[0]
            desc = textwrap.dedent(linkcls.__doc__.rstrip())
            csp = sp.add_parser(name,
                                help=help_str,
                                description=desc,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
            linkcls.add_arguments(csp)

        return p

class ServiceHarness(object):
    """ Harness hosting a REST linking endpoint. """
    Instance = None
    App = Flask(__name__)
    def __init__(self, **kwargs):
        ServiceHarness.Instance = self
        self.host = kwargs.pop('host')
        self.port = int(kwargs.pop('port'))
        self.linker = kwargs.pop('linkcls')(**kwargs)
    
    @staticmethod
    @App.route('/', methods=['POST'])
    def handler():
        data = request.get_json(True)
        response = ServiceHarness.Instance.process(data)

        return Response(json.dumps(response),  mimetype='application/json')

    def __call__(self):
        ServiceHarness.App.run(host='0.0.0.0',port=self.port)

    def read(self, doc):
        return Doc(text=doc['content'],id=doc['id'])

    def process(self, data):
        docs = []
        for doc in data['docs']:
            doc = self.linker(self.read(doc))

            docs.append({
                'id':doc.id,
                'mentions':[{
                    'begin': m.begin,
                    'length': len(m.text),
                    'text': m.text,
                    'resolution': {
                        'score': m.resolution[0][0],
                        'entity': m.resolution[1]
                    }
                } for m in doc.links if m.resolution != None]
            })
            
        return docs

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('host', metavar='HOST')
        p.add_argument('port', metavar='PORT')

        sp = p.add_subparsers()
        for linkcls in LINKER_SET:
            name = linkcls.__name__
            help_str = linkcls.__doc__.split('\n')[0]
            desc = textwrap.dedent(linkcls.__doc__.rstrip())
            csp = sp.add_parser(name,
                                help=help_str,
                                description=desc,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
            linkcls.add_arguments(csp)

        p.set_defaults(cls=cls)
        return p

class BatchLinkingHarness(object):
    def __init__(self, **kwargs):
        self.in_path = kwargs.pop('fname')
        self.out_fh = sys.stdout if 'out_fh' not in kwargs else kwargs.pop('out_fh')
        self.link = kwargs.pop('linkcls')(**kwargs)

    def __call__(self):
        """Link documents """

        with parmapper(self.link, 30, recycle_interval=1) as pm:
            log.info("Starting parallel link...")
            total = 0
            for _, doc in pm.consume(self.read(self.in_path)):
                total += 1
                if total % 100 == 0: 
                    log.info('Linked %i docs...' % total)
                out = self.format(doc).encode('utf-8')
                if out:
                    print >>self.out_fh, out

        log.info('Finished linking documents')

    def read(self, f):
        """Yield Doc objects from path f"""
        with open(f, 'rb') as fh:
            while True:
                try:
                    yield pickle.load(fh)
                except EOFError: break

    def format(self, doc):
        """Format Doc object for printing"""
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('fname', metavar='FILE')
        p.set_defaults(cls=cls)

        sp = p.add_subparsers()
        for linkcls in LINKER_SET:
            name = linkcls.__name__
            help_str = linkcls.__doc__.split('\n')[0]
            desc = textwrap.dedent(linkcls.__doc__.rstrip())
            csp = sp.add_parser(name,
                                help=help_str,
                                description=desc,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
            linkcls.add_arguments(csp)

        return p

class ServerHarness(Harness):
    def __init__(self, port, linker):
        self.port = int(port)
        self.server = HttpServerRequestHandler(
            port = self.port,
            handlers = {
                'POST': self.on_receive_post
            }
        )

    def __call__(self):
        """Link documents """
        log.info("Starting hosted linker...")
        self.server.serve_forever()

    def on_receive_post(self, params):
        return ''.join(self.format(self.link(doc)) for doc in self.read(params))

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('port', metavar='PORT')
        p.add_argument('linker', metavar='LINKER')
        p.set_defaults(cls=cls)
        return p

class TrainingHarness(Harness):
    def __init__(self, **kwargs):
        self.instances = kwargs.pop('instances')       
        super(TrainingHarness, self).__init__(**kwargs)

    def __call__(self):
        """Link documents """
        log.debug("Starting training...")
        for i, doc in enumerate(self.read(self.in_path)):
            self.link(doc)
            log.debug('Finished doc #%i: %s' % (i, doc.id))

        log.debug('Training complete, saving weights..')
        self.link.save_parameters()

    def read(self, f):
        """Yield Doc objects from path f"""
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        super(TrainingHarness, cls).add_arguments(p)
        p.add_argument('instances', metavar='TRAIN_INSTANCE_COUNT', type=int) 
        return p
