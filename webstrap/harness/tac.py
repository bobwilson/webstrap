#!/usr/bin/env python
from schwa import dr
from webstrap.doc import Doc, Annotation
from webstrap.utility.tac_dr_fetch import DrDoc,QueryReader
from webstrap.process.tag import CoreferenceTagger
from webstrap.model.model import Name

import os
import marshal
import cPickle as pickle
import HTMLParser
import re

from .harness import BatchLinkingHarness
import logging

log = logging.getLogger()
ENC = 'utf8'

class Tac(BatchLinkingHarness):
    "Harness for linking TAC documents."
    @staticmethod
    def format_tac_doc(doc):
        return '\n'.join('%s\t%s' % (l.query_id, l.resolution[1]) for l in doc.links if hasattr(l, 'query_id') and l.query_id != None)

    def format(self, doc):
        "Write AIDA-CoNLL formatted output."
        return self.format_tac_doc(doc)

class TacPrepare(object):
    """ Prepare a set of docrep TAC query documents """
    def __init__(self, queries_path, query_docs_path, candidate_model_path, alias_model_path, tac_to_wk_entity_id_map_path, redirect_model_path, doc_model_path, query_gold_annots_path):
        self.queries_path = queries_path
        self.query_gold_annots_path = query_gold_annots_path
        self.query_docs_path = query_docs_path
        self.tac_to_wk_entity_id_map_path = tac_to_wk_entity_id_map_path

        self.name_model = Name(lower=True)
        self.name_model.read('/data0/linking/models/dbpedia.name.model')

        log.info('Loading redirect model...')
        self.redirect_model = marshal.load(open(redirect_model_path,'rb'))

        log.info('Loading candidate model...')
        self.candidate_model = marshal.load(open(candidate_model_path, 'rb'))

        log.info('Loading alias model...')
        alias_model = marshal.load(open(alias_model_path, 'rb'))

        log.info('Appending aliases model...')
        for entity, aliases in alias_model.iteritems():
            for a in aliases:
                a = a.lower()
                if a not in self.candidate_model:
                    self.candidate_model[a] = set()
                self.candidate_model[a].add(entity)

        for alias, entities in self.candidate_model.iteritems():
            n = alias.lower()
            if n in self.name_model.d:

                entities = entities.intersection()

        self.out_path = doc_model_path

    def get_candidates(self, alias):
        alias = alias.lower()
        
        candidates = set()
        if alias in self.candidate_model:
            candidates = set(self.redirect_model.get(c, c) for c in self.candidate_model[alias])

            if alias in self.name_model.d:
                prominent_entities = sorted(self.name_model.d[alias].iteritems(), key=lambda (e,p): p, reverse=True)[:30]
                candidates = candidates.intersection(set(self.redirect_model.get(c, c) for c, _ in prominent_entities))

        return candidates

    def load_tac_query_annots_map(self, path):
        return {parts[0]:parts[1] for parts in (l.decode(ENC).strip().split('\t') for l in open(path, 'r'))}

    def load_tac_to_wk_map(self, path, redirect_model):
        html_parser = HTMLParser.HTMLParser()
        return {k:redirect_model.get(v,v) for k, v in (html_parser.unescape(l.decode(ENC)).strip().split('\t') for l in open(path, 'r'))}

    def mention_over_span(self, doc, span):
        mention_tokens = doc.tokens[span]
        begin = mention_tokens[0].begin
        end = mention_tokens[-1].end
        annotation = Annotation(begin, doc.text[begin:end])
        annotation.doc_token_span = (span.start, span.stop)
        return annotation

    def append_query_mention(self, doc, candidate_model, query, dr_doc, gold_entity):
        """ Convert a tac document to our internal doc representation """
        
        # extreme kludgery to find the query mention in the document (without original doc offsets)
        # todo: use dr objects for our internal doc model to avoid this insanity
        #def tokens_match(qt, dt):
        #    return  qt == dt or \
        #            qt.rstrip('.') in re.findall(r"[\w']+", dt)

        # insanity
        #doc_tokens = [(t.text.replace('-LRB-','(').replace('-RRB-',')').lower(), i) for i, t in enumerate(dr_doc.tokens) if t.text.strip() != '']

        #mention_name = query.name.lower()
        #mention_lparen_idx = mention_name.find('(')
        #if mention_lparen_idx != -1:
        #    mention_name = mention_name[:mention_lparen_idx] # yolo it

        #query_tokens = mention_name.replace('(','( ').replace(')',' )').replace(',',' , ').replace("'s", " 's").replace("s' ", "s ' ").lower().split()
        #query_mention_span = None
        #for i in xrange(len(doc_tokens) - len(query_tokens) + 1):
        #    if all(tokens_match(query_tokens[j], doc_tokens[i+j][0]) for j in xrange(len(query_tokens))):
        #        start = doc_tokens[i][1]
        #        end = doc_tokens[i+len(query_tokens) - 1][1]
        #        query_mention_span = slice(start, end + 1)
        #        break

        #injected = False
        #if query_mention_span == None:
            # some cases query spelling != doc spelling (e.g. defence and defense)
            #log.warn("Couldn't locate mention: (%s) -- [%s]-- (%s)" % (query.name, '|'.join(query_tokens), '|'.join(t for t, _ in doc_tokens)))
            
            # mention is missing? let's just append it to the document as if nothing were out of the ordinary!?
            # may the NLP gods have mercy upon my soul
        #    offset = doc.tokens[-1].begin
        #    for t in query_tokens:
        #        doc.tokens.append(Annotation(offset, t))
        #        offset += len(t) + 1 # space character
        #    query_mention_span = slice(len(doc.tokens) - len(query_tokens), len(doc.tokens))
        #    log.warn('Injecting mention for: %s', ' '.join(query_tokens))
        #    injected = True

        # add a single mention for the query candidate
        #query_mention = self.mention_over_span(doc, query_mention_span)
        #query_mention.query_id = query.id

        query_mention_name = query.name.lower()
        candidates = self.get_candidates(query_mention_name)
        #if len(candidates) == 0:
        #    query_mention_name = query_mention.text.lower()
        #    candidates = self.get_candidates(query_mention_name)

        #if len(candidates) > 0:
        #if gold_entity in candidate_model[query_mention_name]:
        
        
        #def overlaps(a, b):
        #    sa, ea = a.doc_token_span
        #    sb, eb = b.doc_token_span
        #    return sa <= eb and ea >= sb
        #doc.mentions[:] = [m for m in doc.mentions if not overlaps(m, query_mention)]

        if len(doc.mentions) > 0:
            fake_character_offset = doc.mentions[-1].end
            fake_token_offset = doc.mentions[-1].doc_token_span[1]
        else:
            fake_character_offset = len(doc.text) + 1
            fake_token_offset = len(doc.tokens) + 1

        query_mention = Annotation(fake_character_offset + 1, query_mention_name)
        query_mention.query_id = query.id
        query_mention.gold_link = gold_entity
        query_mention.doc_token_span = (fake_token_offset + 1, fake_token_offset + 2)
        doc.mentions.append(query_mention)
        query_mention.candidates = candidates
        doc.gold_links_by_position[(query_mention.begin, query_mention.end)] = gold_entity
        
        #import code
        #code.interact(local=locals())

        if gold_entity != None and gold_entity not in query_mention.candidates:
            log.warn('Entity (%s) not in candidate set for (%s) - (%s)' % (gold_entity.encode(ENC), query.name.encode(ENC), query_mention_name.encode(ENC)))
            for c in query_mention.candidates:
                log.warn('\t%s' % c.encode(ENC))

        return True
        #else:
        #    log.warn('No entities matching alias (%s) (%s)' % (query.name.lower(), query_mention_name))
        #    return False

    def __call__(self):
        """Link documents """
        include_NER_mentions = False

        if self.query_gold_annots_path:
            log.info('Loading gold query annotations...')
            query_to_gold_tac_id = self.load_tac_query_annots_map(self.query_gold_annots_path)
        else:
            query_to_gold_tac_id = {}

        log.info('Loading tac to wikipedia entity mappings...')
        tac_to_wk_id = self.load_tac_to_wk_map(self.tac_to_wk_entity_id_map_path, self.redirect_model)
        
        log.info('Preparing documents...')
        total_queries = 0
        processed_docs = 0
        saved_docs = 0
        no_candidates = 0
        num_no_gold_candidate = 0
        non_nil_queries = 0

        missing_dr_doc_ids = set()

        docs_by_id = {}
        with open(self.out_path, 'wb') as f:
            for query in QueryReader(self.queries_path):
                total_queries += 1

                doc_path = os.path.join(self.query_docs_path, query.doc_id + '.dr')
                try:
                    dr_doc = list(dr.Reader(open(doc_path, 'rb'), DrDoc))[0]
                except IOError:
                    missing_dr_doc_ids.add(query.doc_id)
                    log.debug("Doc for query (%s) not found: %s" % (query.id, doc_path))
                    continue

                gold_tac_entity = query_to_gold_tac_id.get(query.id, None)
                if gold_tac_entity == None or gold_tac_entity[:3] == 'NIL':
                    gold_entity = None
                else:
                    gold_entity = tac_to_wk_id[gold_tac_entity]
                    non_nil_queries += 1
                
                if query.doc_id not in docs_by_id: 
                    d = Doc(' '.join(t.text for t in dr_doc.tokens), query.doc_id)
                    offset = 0
                    d.tokens = []
                    d.mentions = []
                    d.gold_links_by_position = {}

                    for t in dr_doc.tokens:
                        d.tokens.append(Annotation(offset, t.text))
                        offset += len(t.text) + 1 # space character
                     
                    if include_NER_mentions:
                        allowed_entity_types = ['LOCATION','ORGANIZATION','PERSON','MISC']
                        for e in dr_doc.entities:
                            if e.label in allowed_entity_types and e.span.start != e.span.stop:
                                m = self.mention_over_span(d, e.span)

                                candidates = self.get_candidates(m.text)
                                if candidates:
                                    m.candidates = candidates
                                    d.mentions.append(m)
                
                    docs_by_id[query.doc_id] = d

                doc = docs_by_id[query.doc_id]

                if not self.append_query_mention(doc, self.candidate_model, query, dr_doc, gold_entity):
                    no_candidates += 1
                else:
                    if gold_entity != None and gold_entity not in doc.mentions[len(doc.mentions)-1].candidates:
                        num_no_gold_candidate += 1

                if total_queries % 250 == 0:
                    log.debug('Processed %i queries...' % total_queries)
                    #log.debug('Prepared query: %s (%s)' % (query.id, gold_entity))

            coref_tagger = CoreferenceTagger(None)
            for d in docs_by_id.itervalues():
                d = coref_tagger.tag(d)
                for chain in d.chains:
                    mentions_by_length = sorted((m for m in chain.mentions if len(m.candidates) > 0), key=lambda m: len(m.text), reverse=True)
                    
                    # todo: try intersection and fallback to union
                    if mentions_by_length:
                        candidates = set()
                        for m in mentions_by_length:
                            candidates = candidates.union(m.candidates)

                        #longest_mention = mentions_by_length[0]
                        for m in chain.mentions:
                            m.candidates = candidates#longest_mention.candidates

                processed_docs += 1
                if len(d.mentions) > 0:
                    saved_docs += 1
                    pickle.dump(d, f)

                #else:
                #   log.debug('Skipped doc: %s (NIL)' % query.doc_id)

        log.info('Processed %i queries over %i docs (%i non NIL queries, %i docs used)' % (total_queries, processed_docs, non_nil_queries, saved_docs))
        log.info('%i queries without gold link in candidate set' % (num_no_gold_candidate))
        log.info('%i queries with no candidates' % (no_candidates))

        log.debug('Missing docs:\n%s' % '\n'.join(missing_dr_doc_ids))

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('queries_path', metavar='QUERIES_MODEL_PATH')
        p.add_argument('query_docs_path', metavar='QUERY_DOCS_DIR')
        p.add_argument('candidate_model_path', metavar='CANDIDATE_MODEL')
        p.add_argument('alias_model_path', metavar='ALIAS_MODEL')
        p.add_argument('tac_to_wk_entity_id_map_path', metavar='TAC_TO_WK_ENTITY_ID_MAP_PATH')
        p.add_argument('redirect_model_path', metavar='REDIRECT_MODEL_PATH')
        p.add_argument('doc_model_path', metavar='DOC_MODEL')
        p.add_argument('--query_gold_annots_path', required=False, metavar='QUERY_GOLD_ANNOTS_PATH')
        p.set_defaults(cls=cls)
        return p
