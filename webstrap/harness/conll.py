from .harness import BatchLinkingHarness
from ..doc import Doc
import cPickle as pickle
import logging

log = logging.getLogger()

class Conll(BatchLinkingHarness):
    "Harness for linking CoNLL03 AIDA-YAGO2 dataset."

    DOCSTART_MARKER = '-DOCSTART-'

    @staticmethod
    def iter_read_conll_docs(path, doc_id_predicate, max_docs = None):
        """
        Read AIDA-CoNLL format, e.g.:

        -DOCSTART- (1 EU)
        EU  B   EU  --NME--
        rejects
        German  B   German  Germany http://en.wikipedia.org/wiki/Germany    11867   /m/0345h
        call
        to
        boycott
        British B   British United_Kingdom  http://en.wikipedia.org/wiki/United_Kingdom 31717   /m/07ssc
        lamb
        .
        """
        with open(path, 'rd') as f:
            doc_id = None
            doc_tokens = None
            doc_mentions = None
            doc_count = 0
            for line in f:
                parts = line.decode('utf-8').split('\t')
                if len(parts) > 0:
                    token = parts[0].strip()

                    # if this line contains a mention
                    if len(parts) >= 4 and parts[1] == 'B':
                        # filter empty and non-links
                        if parts[3].strip() != '' and not parts[3].startswith('--'):
                            entity = parts[3]
                            begin = sum(len(t)+1 for t in doc_tokens)
                            position = (begin, begin + len(parts[2]))
                            doc_mentions.append((entity, position))

                    if token.startswith(Conll.DOCSTART_MARKER):
                        if doc_id != None and doc_id_predicate(doc_id):
                            doc_count += 1
                            yield Doc(' '.join(doc_tokens), doc_id, doc_mentions)

                            if max_docs != None and doc_count >= max_docs:
                                doc_id = None
                                break 
                                
                        doc_id = token[len(Conll.DOCSTART_MARKER)+2:-1]
                        doc_tokens = []
                        doc_mentions = []
                    elif doc_id != None:
                        doc_tokens.append(token)

            if doc_id != None and doc_id_predicate(doc_id):
                yield Doc(' '.join(doc_tokens), doc_id, doc_mentions)

    @staticmethod
    def format_doc_link(doc, link, token, token_offset):
        pos = 'B' if link.doc_token_span[0] == token_offset else 'I'
        return '\t'.join([token.text, pos, link.text, link.resolution[1]])

    @staticmethod
    def format_doc(doc):
        "Write AIDA-CoNLL formatted output."
        links_by_index = { l.begin : l for l in doc.links }

        lines = [Conll.DOCSTART_MARKER + ' (' + doc.id + ')']

        link = None
        for i, token in enumerate(doc.tokens):
            link = link or links_by_index.get(token.begin, None)

            if link:
                lines.append(Conll.format_doc_link(doc, link, token, i))
            else:
                lines.append(token.text)

            if link != None and i+1 >= link.doc_token_span[1]: link = None

        return '\n'.join(lines) + '\n'

    def format_link(self, doc, link, token, token_offset):
        return self.format_doc_link(doc, link, token, token_offset)
    
    """
    def read(self, path):
        return self.iter_read_conll_docs(path, lambda doc_id: 'test' in doc_id)
    """
    
    def format(self, doc):
        "Write AIDA-CoNLL formatted output."
        return self.format_doc(doc)

import marshal
from ..process.tokenise import RegexTokeniser, NON_WHITESPACE_RE
from ..process.tag import GoldTagger

class ConllPrepare(object):
    """ Tokenise and tag a conll document """
    def __init__(self, conll_data_path, doc_type, alias_model_path, redirect_model_path, doc_model_path):
        self.in_path = conll_data_path
        self.out_path = doc_model_path
        self.doc_predicate = {
            'train': self.is_training_doc,
            'dev': self.is_dev_doc,
            'test': self.is_test_doc
        }[doc_type]

        self.redirect_model_path = redirect_model_path
        self.alias_model_path = alias_model_path

    @staticmethod
    def is_training_doc(doc_id):
        return 'test' not in doc_id
    @staticmethod
    def is_test_doc(doc_id):
        return 'testb' in doc_id
    @staticmethod
    def is_dev_doc(doc_id):
        return 'testa' in doc_id

    def __call__(self):
        """Link documents """
        log.info('Loading redirect model...')
        redirect_model = marshal.load(open(self.redirect_model_path,'rb'))

        log.info('Initialising processors...')
        tokeniser = RegexTokeniser(NON_WHITESPACE_RE)

        alias_model = marshal.load(open(self.alias_model_path, 'rb'))
        entities_by_name = {}
        for entity, names in alias_model.iteritems():
            for name in (n.lower() for n in names):
                entities_by_name.setdefault(name, set()).add(entity)
        
        entities_by_name = {n:list(e) for n, e in entities_by_name.iteritems()}

        tagger = GoldTagger(entities_by_name, exclude_unlinkables = False)

        log.info('Preparing documents...')
        total = 0
        with open(self.out_path, 'wb') as f:
            for doc in Conll.iter_read_conll_docs(self.in_path, self.doc_predicate):
                pickle.dump(tagger(tokeniser(doc)), f)
                total += 1
                if total % 50 == 0:
                    log.info('Prepared %i docs...' % total)

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('conll_data_path', metavar='CONLL_DATA')
        p.add_argument('doc_type', metavar='DOC_TYPE', choices='train,dev,test')
        p.add_argument('alias_model_path', metavar='CANDIDATE_MODEL')
        p.add_argument('redirect_model_path', metavar='REDIRECT_MODEL')
        p.add_argument('doc_model_path', metavar='DOC_MODEL')
        p.set_defaults(cls=cls)
        return p