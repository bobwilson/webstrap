#!/usr/bin/env python
import sys
import logging

log = logging.getLogger()

class Evaluation(object):
    def __init__(self, labels, out_fh=sys.stdout):
        self.labels_path = labels
        self.out_fh = out_fh

    def __call__(self):
        """Evaluate results """
        with open(self.labels_path, 'r') as labels_f:
            result = self.eval(sys.stdin, labels_f)

        print >> self.out_fh, self.format(result)

    def eval(self, e_path, g_path):
        """Evaluate input against labels"""
        raise NotImplementedError

    def format(self, doc):
        """Format results for printing"""
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('labels', metavar='LABELS_FILE')
        p.set_defaults(cls=cls)
        return p