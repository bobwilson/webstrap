import cgi
import urlparse

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from StringIO import StringIO 

import logging

log = logging.getLogger()

class DelegatedRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):  self.invoke_handler('POST')
    def do_GET(self):   self.invoke_handler('GET')

    def invoke_handler(self, name):
        if name in self.server.handlers:
            self.write_response(self.server.handlers[name](self.get_params()))
        else:
            self.empty_response(404)

    def log_message(self, format, *args):
        log.debug(format % args)
        return

    def empty_response(self, status = 200):
        self.send_response(status)
        self.end_headers()

    def write_response(self, response):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(response)

    def get_params(self):
        postvars = {}
        ctype, pdict = cgi.parse_header(self.headers['content-type'])
        length = int(self.headers['content-length'])

        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(StringIO(self.rfile.read(length)), pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            postvars = urlparse.parse_qs(self.rfile.read(length), keep_blank_values=1)

        return { k:v[0] for k, v in postvars.iteritems() }

class HttpServerRequestHandler(HTTPServer):
    def __init__(self, endpoint = '', port = 8080, handlers = dict()):
        HTTPServer.__init__(self, (endpoint, port), DelegatedRequestHandler)
        self.handlers = handlers