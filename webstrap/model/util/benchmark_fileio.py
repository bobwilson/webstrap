#!/usr/bin/env python 
"Benchmark dictionary serialisation times"

# WONT include ujson (not clearly better in benchmarks, additional dependency)

from timeit import timeit
from webstrap.model.model import Name

MODELD = 'models/wikipedia'
name = Name()
name.read(MODELD)
d_tups = {n:tuple(d.iteritems()) for n,d in name.d.iteritems()}

import_name = 'from __main__ import name, d_tups'
cpickle_setup = 'import cPickle; %s' % import_name
cpickle_write_dict = 'cPickle.dump(name.d, open("named.pkl", "wb"), protocol=2)'
cpickle_write_tups = 'cPickle.dump(name.d, open("namet.pkl", "wb"), protocol=2)'
cpickle_read_dict = 'cPickle.load(open("named.pkl", "rb"))'
cpickle_read_tups = 'cPickle.load(open("namet.pkl", "rb"))'
marshal_setup = 'import marshal; %s' % import_name
marshal_write_dict = 'marshal.dump(name.d, open("named.msh", "wb"))'
marshal_write_tups = 'marshal.dump(d_tups, open("namet.msh", "wb"))'
marshal_read_dict = 'marshal.load(open("named.msh", "rb"))'
marshal_read_tups = 'marshal.load(open("namet.msh", "rb"))'
msgpack_setup = 'import msgpack; %s' % import_name
msgpack_write_dict = 'msgpack.dump(name.d, open("named.mpk", "wb"))'
msgpack_write_tups = 'msgpack.dump(d_tups, open("namet.mpk", "wb"))'
msgpack_read_dict = 'msgpack.load(open("named.mpk", "rb"))'
msgpack_read_tups = 'msgpack.load(open("namet.mpk", "rb"))'
custfmt_setup = import_name
cstfmt0_write_dict = 'name.write("models/wp0")'
cstfmt0_read_dict = 'name.read("models/wp0")'
cstfmt1_write_dict = 'name.write1("models/wp1")'
cstfmt1_read_dict = 'name.read1("models/wp1")'

tests = [
    # (label, setup, write, read)
    ('cpickle/dict', cpickle_setup, cpickle_write_dict, cpickle_read_dict),
    ('cpickle/tups', cpickle_setup, cpickle_write_tups, cpickle_read_tups),
    ('marshal/dict', marshal_setup, marshal_write_dict, marshal_read_dict),
    ('marshal/tups', marshal_setup, marshal_write_tups, marshal_read_tups),
    ('msgpack/dict', msgpack_setup, msgpack_write_dict, msgpack_read_dict),
    ('msgpack/tups', msgpack_setup, msgpack_write_tups, msgpack_read_tups),
    ('cstfmt0/dict', custfmt_setup, cstfmt0_write_dict, cstfmt0_read_dict),
    ('cstfmt1/dict', custfmt_setup, cstfmt1_write_dict, cstfmt1_read_dict),
    ]

loops = 10

col1len, col2len = 12, 7
col1lab = '%%-%ds' % col1len
colnlab = '%%%ds' % col2len
colnval = '%%%d.3f' % col2len

print '%s %s %s %s' % (col1lab % 'Command/Data',
                       colnlab % 'Write',
                       colnlab % 'Read',
                       colnlab % 'Total')
print '%s %s %s %s' % ('-'*col1len, '-'*col2len, '-'*col2len, '-'*col2len)

for label, setup, write_test, read_test in tests:
    print label,
    w = timeit(write_test, setup, number=loops) / loops
    print colnval % w,
    r = timeit(read_test, setup, number=loops) / loops
    print colnval % r,
    tot = w + r
    print colnval % tot
