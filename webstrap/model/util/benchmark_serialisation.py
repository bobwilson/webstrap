#!/usr/bin/env python 
"Benchmark dictionary serialisation times"

# WONT include ujson

from timeit import timeit
from webstrap.model.model import Name

MODELD = 'models/wikipedia'
name = Name()
name.read(MODELD)
d_tups = {n:tuple(d.iteritems()) for n,d in name.d.iteritems()}

import_name = 'from __main__ import name, d_tups'
cpickle_setup = 'import cPickle; %s' % import_name
cpickle_encode_dict = 'cPickle.dumps(name.d, protocol=2)'
cpickle_encode_tups = 'cPickle.dumps(d_tups, protocol=2)'
cpickle_decode = 'cPickle.loads(src)'
marshal_setup = 'import marshal; %s' % import_name
marshal_encode_dict = 'marshal.dumps(name.d)'
marshal_encode_tups = 'marshal.dumps(d_tups)'
marshal_decode = 'marshal.loads(src)'
msgpack_setup = 'import msgpack; %s' % import_name
msgpack_encode_dict = 'msgpack.dumps(name.d)'
msgpack_encode_tups = 'msgpack.dumps(d_tups)'
msgpack_decode = 'msgpack.loads(src)'

tests = [
    # (label, setup, encode, decode)
    ('cpickle/dict', cpickle_setup, cpickle_encode_dict, cpickle_decode),
    ('cpickle/tups', cpickle_setup, cpickle_encode_tups, cpickle_decode),
    ('marshal/dict', marshal_setup, marshal_encode_dict, marshal_decode),
    ('marshal/tups', marshal_setup, marshal_encode_tups, marshal_decode),
    ('msgpack/dict', msgpack_setup, msgpack_encode_dict, msgpack_decode),
    ('msgpack/tups', msgpack_setup, msgpack_encode_tups, msgpack_decode),
    ]

loops = 1

col1len, col2len = 12, 6
col1fmt = '%%-%ds' % col1len
col2fmt = '%%%d.3f' % col2len
print '%s %s %s %s' % ('Command/Data', 'Encode', 'Decode', ' Total')
print '%s %s %s %s' % ('-'*col1len, '-'*col2len, '-'*col2len, '-'*col2len)

for label, encode_setup, encode_test, decode_test in tests:
    enc = timeit(encode_test, encode_setup, number=loops) / loops
    decode_setup = '%s; src=%s' % (encode_setup, encode_test)
    dec = timeit(decode_test, decode_setup, number=loops) / loops
    tot = enc + dec
    print '%s %s %s %s' % (label, col2fmt % enc, col2fmt % dec, col2fmt % tot)
