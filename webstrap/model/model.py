#!/usr/bin/env python
from __future__ import print_function

from collections import Counter, defaultdict
from functools32 import lru_cache
from msgpack import Packer, Unpacker
import marshal
import operator
import cPickle as pickle
import numpy as np
import math
import os
import logging
import mmap

log = logging.getLogger()

ENC = 'utf8'
FMT_MARSHAL='marshal'
FMT_TSV='tsv'

class Name(object):
    def __init__(self, lower=False):
        self.lower = lower
        self.d = {}

    def entities(self, name):
        "Return list of (score, entity) tuples for name."
        return [(s,e) for e,s in self.d.get(name, {}).iteritems()]

    def get_entities_by_name(self):
        return {n : e.keys() for n,e in self.d.iteritems()}

    def score(self, name, entity, candidates):
        "Return score for name=>entity, default 0.0."
        entities = self.d.get(name, {})
        
        if entity in entities:
            return entities[entity]
        return 1e-10

        #if len(entities) == 0:
        #    return 1.0 / len(candidates)
        #else:
        #    p_e_n = entities.get(entity, 0.0)
        #    return ((1.0 / len(candidates)) + p_e_n) / (1.0 + sum(entities.get(e, 0.0) for e in candidates))
        
        
        #if len(entities) == 0:
        #    return 1e-20
        #else:
        #    p_e_n = entities.get(entity, 0.0)
        #    return ((1.0 / len(entities)) + p_e_n) / (1.0 + sum(entities.values()))

    def normalise(self):
        for counts in self.d.itervalues():
            total = float(sum(counts.itervalues()))
            for e in counts.iterkeys():
                counts[e] /= total

    def iternames(self):
        return self.d.iterkeys()

    def update(self, name, entity, score):
        "Add score for name=>entity."
        self.d.setdefault(name, {})[entity] = score

    def write(self, path):
        "Write model to file handle."
        log.debug('Writing model for %i names to file: %s ...' % (len(self.d), path))

        with open(path, 'wb') as fh:
            for name, d in self.d.iteritems():
                marshal.dump((name, d), fh)

    def read(self, path):
        "Read model from file."
        log.debug('Loading name model from: %s ...' % path)

        with open(path, 'rb') as fh:
            while True:
                try:
                    name, d = marshal.load(fh)
                    norm = self._norm(name)
                    self.d[norm] = self._dsum(norm, d)
                except EOFError: break

    def _norm(self, name):
        if self.lower:
            name = name.lower()
        return name

    def _dsum(self, name, d):
        if name in self.d:
            c = Counter(self.d[name]) + Counter(d)
            total = sum(c.values())
            d = {n:v/total for n,v in c.iteritems()}
        return d

class DictionaryModel(object):
    def __init__(self, model_fname, encoding):
        self.d = {}
        self.model_fname = model_fname
        self.enc = encoding

    def write(self, path):
        "Write model to file under path."
        packer = Packer(encoding=self.enc)
        with open(os.path.join(path, self.model_fname), 'wb') as fh:
            for k, v in self.d.iteritems():
                fh.write(packer.pack((k, v)))

    def read(self, model_path):
        "Read model from file."
        f = os.path.join(model_path, self.model_fname)
        
        log.debug('Loading model: %s ...' % f)
        unpacker = Unpacker(open(f, 'rb'), encoding=self.enc)
        self.d = {k:v for k,v in unpacker}

class Entity(object):
    """ Entity prior. """

    # entity serialization format constants
    ENTITY_FMT_DEFAULT=FMT_MARSHAL

    def __init__(self):
        self.d = {}

    def normalise(self):
        total = float(sum(self.d.itervalues()))
        for k in self.d.iterkeys():
            self.d[k] /= total

    def score(self, entity):
        "Return score for entity, default 1e-20."
        return max(1e-20, self.d.get(entity, 0))

    def update(self, entity, score):
        "Add score for entity."
        self.d[entity] = score

    def write(self, path, fmt=ENTITY_FMT_DEFAULT):
        log.info('Writing %s model for %i entities to file: %s ...', fmt, len(self.d), path)
        with open(path, 'wb') as f:
            if fmt == FMT_MARSHAL:
                marshal.dump(self.d, f)
            elif fmt == FMT_TSV:
                print('entity\tprior'.encode(ENC), file=f)
                for entity, value in self.d.iteritems():
                    out = '%s\t%s' % (entity, str(value))
                    print(out.encode(ENC), file=f)

    def read(self, path, fmt=ENTITY_FMT_DEFAULT):
        log.info('Reading %s entity model from file: %s ...', fmt, path)
        with open(path, 'rb') as f:
            if fmt == FMT_MARSHAL:
                self.d = marshal.load(f)
            elif fmt == FMT_TSV:
                self.d = {}
                f.readline()
                for l in f:
                    k, v = l.decode(ENC).split('\t')
                    self.d[k] = float(v)

class EntityCooccurrence(object):
    def __init__(self, cooccurrence_counts = None, occurrence_counts = None):
        self.cooccurrence_counts = cooccurrence_counts or {}
        self.occurrence_counts = occurrence_counts or {}

    def cooccurrence_count(self, a, b):
        if a > b:
            a, b = b, a

        # this crazy stacked dictionary method turns out to be much, much more memory/time efficient
        # than using the frozensets of entity pairs for the key.
        # perhaps because memory allocation for the dict blows up when you have lots of keys
        return self.cooccurrence_counts.get(a, {}).get(b, 0)

    def conditional_probability(self, a, b):
        intersection = self.cooccurrence_count(a, b)
        return 0.0 if intersection == 0 else intersection / self.occurrence_counts[b]

    def write(self, path):
        log.info('Writing cooccurrence model to file: %s' % path)
        with open(path, 'wb') as f:
            marshal.dump(self.cooccurrence_counts, f)
            marshal.dump(self.occurrence_counts, f)

    @staticmethod
    def read(path):
        log.info('Reading entity cooccurrence model from file: %s' % path)

        with open(path, 'rb') as f:
            cooccurrence_counts = marshal.load(f)
            occurrence_counts = marshal.load(f)

        return EntityCooccurrence(cooccurrence_counts, occurrence_counts)

class EntityOccurrence(object):
    "Inlinks to an entity article."
    SIZEW = 4527417 # http://en.wikipedia.org/wiki/Wikipedia:Size_of_Wikipedia
    LOGW = math.log(SIZEW)

    def __init__(self, entity_occurences = None):
        self.d = {} if entity_occurences == None else entity_occurences

    def add(self, entity, url):
        self.d.setdefault(entity, set()).add(url)

    def occurrences(self, entity):
        return self.d.get(entity, set())

    @lru_cache(maxsize=1000000)
    def entity_relatedness(self, a, b):
        """ Milne relatedness of two entities. """
        occ_a = self.occurrences(a)
        occ_b = self.occurrences(b)
        occ_common = occ_a.intersection(occ_b)

        try:
            logmax = max(len(occ_a), len(occ_b))
            logmin = min(len(occ_a), len(occ_b))
            logint = len(occ_common)
            return (logmax - logint) / (self.LOGW - logmin)
        except ValueError:
            return 0.0

    def tagme_vote(self, e1, candidate_set):
        """
        Return score for entity e1 based on another anchor's candidates
        (Ferragina & Scaiella, 2010).
        NOTE: For F&S, candidates should have p(entity|name) scores.
        candidate_set - set of (score, entity) tuples
        """
        try:
            return sum([self.entity_relatedness(e1, e2) * score
                        for score, e2 in candidate_set]) / len(candidate_set)
        except ZeroDivisionError:
            return 0.0

    def tagme_score(self, e1, candidate_sets):
        """
        Return total relevance for entity e1 across other anchors
        (Ferragina & Scaiella, 2010).
        candidate_sets - list of candidate sets, unique by anchor
        """
        return sum([self.tagme_vote(e1, a2) for a2 in candidate_sets], 0.0)

    def write(self, path):
        log.info('Writing occurrence model to file: %s' % path)
        marshal.dump(self.d, open(path, 'wb'))

    @staticmethod
    def read(path):
        log.info('Reading entity occurrence model from file: %s' % path)
        return EntityOccurrence(marshal.load(open(path, 'rb')))

class Links(object):
    "Outlinks from entity article."
    def __init__(self, outlinks = None):
        self.d = {} if outlinks == None else outlinks

    def get(self, source):
        return self.d.get(source, set())

    def update(self, source, target):
        self.d.setdefault(source, set()).add(target)

    def iteritems(self):
        return self.d.iteritems()
    
    def write(self, path):
        log.info('Writing links model to file: %s' % path)
        marshal.dump(self.d, open(path, 'wb'))

    @staticmethod
    def read(path):
        log.info('Reading links model from file: %s' % path)
        return Links(marshal.load(open(path, 'rb')))

class Redirects(object):
    def __init__(self, redirects = None):
        self.d = {} if redirects == None else redirects

    def update(self, source, target):
        self.d[source] = target

    def map(self, entity):
        return self.d.get(entity, entity)

    def get_redirects_by_entity(self):
        redirects = defaultdict(list)
        for source, target in self.d.iteritems():
            redirects[target].append(source)
        return redirects

    def write(self, path):
        log.debug('Writing redirect model for %i entity ids to file: %s ...' % (len(self.d), path))
        with open(path, 'wb') as f:
            marshal.dump(self.d, f)

    @staticmethod
    def read(path):
        log.debug('Reading redirect model from file: %s ...' % path)
        with open(path, 'rb') as f:
            return Redirects(marshal.load(f))

class HitCount(DictionaryModel):
    "Model of wikipedia hit counts for an entity."
    def __init__(self):
        super(HitCount, self).__init__(HITCOUNT_MODEL, ENC)

    def update(self, entity, count):
        self.d[entity] = count

    def iteritems(self):
        return self.d.iteritems()

    def count(self, entity):
        "Return the view count for an entity, default 0"
        return self.d.get(entity, 0)

    def size(self):
        return len(self.d.items())

class ErdEntity(DictionaryModel):
    "Entity collection for the ERD task."
    def __init__(self):
        super(ErdEntity, self).__init__(ERDENTITY_MODEL, ENC)

    def contains_wkid(self, wk_id):
        return wk_id in self.d

    def names(self):
        return set(n for _, _, n in self.iteritems())

    def iteritems(self):
        return ((wk_id, fb_id, fb_name) for wk_id, (fb_id, fb_name) in self.d.iteritems())

    def update(self, wk_id, fb_id, fb_name):
        self.d[wk_id] = (fb_id, fb_name)

class WordVectors(object):
    def __init__(self, vocab, vectors):
        self.vocab = vocab
        self.vectors = vectors

    def vocab_size(self):
        return self.vectors.shape[0]

    def vector_size(self):
        return self.vectors.shape[1]

    def word_to_vec(self, word):
        return self.vectors[self.vocab[word]] if word in self.vocab else None

    def write(self, path):
        "Write model to file under path."
        log.debug('Writing %id word vector model for %s words...' % (self.vector_size(), self.vocab_size()))
        with open(path, 'wb') as f:
            marshal.dump(self.vocab, f)
            np.save(f, self.vectors)

    @staticmethod
    def read(model_path):
        "Read model from file."
        log.debug('Loading word vector model from: %s', model_path)
        with open(model_path, 'rb') as f:
            return WordVectors(marshal.load(f), np.load(f))

class EntityTermCounts(object):
    "Model of term counts for a set of entities."
    def __init__(self):
        self.entity_term_counts = defaultdict(dict)

    def update(self, entity, term, count):
        self.entity_term_counts[entity][term] = count

    def get(self, entity, term):
        if entity not in self.entity_term_counts:
            return 0.0
        
        return self.entity_term_counts[entity].get(term, 0.0)

    def get_terms(self, entity):
        return self.entity_term_counts[entity]

    def write(self, path):
        "Write model to file."
        
        log.debug('Writing term frequency model to file: %s ...' % path)

        with open(path, 'wb') as f:
            for entity, counts in self.entity_term_counts.iteritems():
                marshal.dump((entity, counts), f)

    def read(self, path):
        "Read model from file."

        log.debug('Loading raw model from file: %s ...' % path)

        with open(path, 'rb') as fh:
            while True:
                try:
                    entity, counts = marshal.load(fh)
                    self.entity_term_counts[entity] = counts
                except EOFError: break

class mmdict(object):
    def __init__(self, path):
        self.path = path
        self.index = {}
        
        index_path = self.path + '.index'
        log.debug('Loading memory mapped dictionary: %s ...' % index_path)
        with open(index_path, 'rb') as f:
            while True:
                try:
                    key, offset = self.deserialise(f)
                    self.index[key] = offset
                except EOFError: break

        self.data_file = open(path + '.data', 'rb')
        self.data_mmap = mmap.mmap(self.data_file.fileno(), 0, prot=mmap.PROT_READ)
    
    @staticmethod
    def serialise(obj, f):
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def deserialise(f):
        return pickle.load(f)

    @staticmethod
    def static_itervalues(path):
        with open(path + '.data', 'rb') as f:
            while True:
                try:
                    yield mmdict.deserialise(f)
                except EOFError: break      

    def iteritems(self):
        sorted_idx = sorted(self.index.iteritems(), key=operator.itemgetter(1))
        
        for i, v in enumerate(self.itervalues()):
            yield (sorted_idx[i][0], v)

    def iterkeys(self):
        return self.index.iterkeys()

    def itervalues(self):
        self.data_mmap.seek(0)
        while True:
            try:
                yield self.deserialise(self.data_mmap)
            except EOFError: break

    def __len__(self):
        return len(self.index)

    def __contains__(self, key):
        return key in self.index

    def __getitem__(self, key):
        if key not in self:
            return None

        self.data_mmap.seek(self.index[key])
        return self.deserialise(self.data_mmap)

    def __enter__(self):
        return self

    def close(self):
        if hasattr(self, 'data_mmap') and self.data_mmap != None:
            self.data_mmap.close()
        if hasattr(self, 'data_file') and self.data_file != None:
            self.data_file.close()

    def __exit__(self, type, value, traceback):
        self.close()

    def __del__(self):
        self.close()

    @staticmethod
    def write(path, iter_kvs):
        with open(path + '.index','wb') as f_index, open(path + '.data', 'wb') as f_data:
            for key, value in iter_kvs:
                mmdict.serialise((key,f_data.tell()), f_index)
                mmdict.serialise(value, f_data)

class EntityMentionContexts(object):
    "Simple model for entity mentions and context."
    def __init__(self):
        self.entity_mentions = dict()

    def add(self, entity, source, left, sf, right):
        mention = (source, left, sf, right)
        self.entity_mentions.setdefault(entity, []).append(mention)

    def entity_count(self):
        return len(self.entity_mentions)

    def iter_entities(self):
        return self.entity_mentions.iterkeys()

    def mention_count(self):
        return sum(len(ms) for ms in self.entity_mentions.itervalues())

    def mentions(self, entity):
        return self.entity_mentions.get(entity, [])

    def iteritems(self):
        for entity, mentions in self.entity_mentions.iteritems():
            for mention in mentions:
                yield (entity, mention)

    def get_entities_by_surface_form(self):
        entities_by_name = defaultdict(set)

        for entity, mentions in self.entity_mentions.iteritems():
            for _, _, name, _ in mentions:
                entities_by_name[name.lower()].add(entity)

        return entities_by_name

    def write(self, path):
        "Write model to file."

        log.debug(
            'Writing wikilinks context model (%i entities, %i mentions): %s ...' 
            % (self.entity_count(), self.mention_count(), path))
        
        with open(path, 'wb') as f:
            #marshal.dump(self.term_occurences, f)
            for entity, mentions in self.entity_mentions.iteritems():
                marshal.dump((entity, mentions), f)

    @staticmethod
    def iter_entity_mentions_from_path(path):
        with open(path, 'rb') as fh:
            #marshal.load(fh) # todo: remove after model rebuilt
            while True:
                try:
                    yield marshal.load(fh)
                except EOFError: break

    @staticmethod
    def read(path):
        "Read model from file."
        emc = EntityMentionContexts()

        log.debug('Loading mention context model: %s ...' % path)
        with open(path, 'rb') as fh:
            #marshal.load(fh) # todo: remove after model rebuilt
            while True:
                try:
                    entity, mentions = marshal.load(fh)
                    emc.entity_mentions[entity] = mentions
                except EOFError: break

        return emc
