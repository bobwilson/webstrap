import gzip
import logging
import numpy

from ..model import WordVectors

log = logging.getLogger()

class BuildWordVectors(object):
    "Build a word vector model from a binary word2vec file."
    def __init__(self, inpath, outpath):
        self.in_path = inpath
        self.out_path = outpath

    def __call__(self):
        self.build(self.in_path).write(self.out_path)

    def build(self, path):
        vocab = dict()

        with gzip.open(path) as f:
            vocab_sz, vec_sz = (int(c) for c in f.readline().split())
            
            log.debug('Reading %id word2vec model for %i words...' % (vec_sz, vocab_sz))

            vectors = numpy.empty((vocab_sz, vec_sz), dtype=numpy.float)
            vec_byte_len = numpy.dtype(numpy.float32).itemsize * vec_sz

            for line_idx in xrange(vocab_sz):
                word = ''
                while True:
                    ch = f.read(1)
                    if ch == ' ': break
                    if ch != '\n': word += ch

                vocab[word] = line_idx
                vectors[line_idx] = numpy.fromstring(f.read(vec_byte_len), numpy.float32)

        return WordVectors(vocab, vectors)

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('inpath', metavar='INFILE')
        p.add_argument('outpath', metavar='OUTFILE')
        p.set_defaults(cls=cls)
        return p