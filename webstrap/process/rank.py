#!/usr/bin/env python
from ..model.model import Entity
from .process import Process
from scipy.stats import hmean
from itertools import chain,combinations

import math
import numpy
import logging

log = logging.getLogger()

# TODO Generalise backoff and interpolation for arbitrary rankers
"""
    def backoff(self, entity, name):
        return self.cond(entity, name) or self.prob(entity)

    def interpolate(self, entity, name, weight=0.5):
        return weight*self.cond(entity, name) + (1-weight)*self.prob(entity)
"""

class Ranker(Process):
    def __call__(self, doc):
        assert hasattr(doc, 'mentions'), 'doc must have mentions'
        doc.mentions = list(self._rank(doc))
        return doc

    def _rank(self, doc):
        for m in doc.mentions:
            m.candidates = sorted(self._score(m, doc), reverse=True)
            yield m

    def _score(self, mention, doc):
        "Yields (score, candidate) tuples in order."
        raise NotImplementedError

    # TODO kill - normalisation and smoothing sould be in model
    def _sns(self, mention, doc):
        return self._smooth(self._norm(self._score(mention, doc)))

    def _norm(self, candidates):
        total = sum([s for s,c in candidates], 0.0)
        for s, c in candidates:
            score = s/total if s > 0 else 0.0
            yield score, c

    def _smooth(self, candidates):
        candidates = list(candidates)
        prior = 1.0 / len(candidates)
        for s, c in candidates:
            score = (prior + s)/2
            yield score, c

# TODO Kill
class DummyRanker(Ranker):
    def _score(self, mention, doc):
        yield (1.0, '/m/0fd4x')

class EntityRanker(Ranker):
    def __init__(self, model_path):
        self.entity = Entity()
        self.entity.read(model_path)

    def _score(self, mention, doc):
        for s,c in mention.candidates:
            yield self.entity.score(c), c

class HitCountRanker(Ranker):
    def __init__(self, hitcount_model):
        self.entity_counts = hitcount_model
        self.log_max_count = math.log10(max(c for _, c in self.entity_counts.iteritems()))

    def _score(self, mention, doc):
        """ Returns list of (score, candidate)"""

        # log.debug('Hit Count Ranking: [%s] for doc [%s]...' % (mention.text, doc.text[:15] + '...'))
        # todo: proper smoothing of counts
        # todo: better handling of entity references
        return ((math.log10(self.entity_counts.count(e) + 1.0) / (self.log_max_count + 1.0), e) for e in mention.candidates)

class Backoff(Ranker):
    def __init__(self, rankers):
        """
        rankers - ordered list of rankers
        """
        self.rankers = [r() for r in rankers]

    def _score(self, mention, doc):
        scores = [r._score(mention, doc)[0] for r in self.rankers]
        for i in range(len(mention.candidates)):
            score = 0.0
            for j in range(len(scores)):
                if scores[i][j] > score:
                    score = scores[i][j]
                    break
            yield score, mention.candidates[i]

# TODO generalise to handle context-driven rankers
class Interpolator(Ranker):
    def __init__(self, rankers):
        """
        rankers - list of (weight, ranker) tuples
        """
        self.rankers = [(w,r()) for w,r in rankers]

    def _score(self, mention, doc):
        scores = zip(self._weighted(mention, doc))
        for i in range(len(mention.candidates)):
            yield sum(scores[i]), mention.candidates[i]

    def _weighted(self, mention, doc):
        for weight, ranker in self.rankers:
            for score, _ in ranker._score(mention, doc):
                yield weight*score

class CombinedRanker(Ranker):
    def __init__(self, rankers, include_multiplied_scores = True):
        self.rankers = rankers

        self.include_multiplied_scores = include_multiplied_scores
        if self.include_multiplied_scores:
            self.num_scores = 2**len(rankers) - 1
        else:
            self.num_scores = len(rankers)

    def combinator(self, mention, candidate, doc, scores):
        raise NotImplementedError

    @staticmethod
    def multiply(seq):
        return reduce(lambda acc, s: acc * s, seq, 1.0)

    @staticmethod
    def iter_powerset(items): # powerset - w/o empty set
        return chain(*(combinations(items, sz) for sz in xrange(1, len(items)+1)))

    def _score(self, mention, doc):
        ranker_candidate_scores = [list(r._score(mention, doc)) for r in self.rankers]

        mention.candidate_scores = {}

        for i in xrange(0, len(mention.candidates)):
            candidate = mention.candidates[i]
            scores = [s[i][0] for s in ranker_candidate_scores]
            mention.candidate_scores[candidate] = scores

            if self.include_multiplied_scores:
                #scores = [-math.log(max(self.multiply(c), 0.00001)) for c in self.iter_powerset(scores)]
                scores = [self.multiply(c) for c in self.iter_powerset(scores)]

            yield (self.combinator(mention, candidate, doc, scores), candidate)

class MultiplicativeRanker(CombinedRanker):
    def combinator(self, mention, candidate, doc, scores):
        return self.multiply(scores)

class HarmonicRanker(CombinedRanker):
    def combinator(self, mention, candidate, doc, scores):
        return hmean(scores)

class LogLinearRanker(CombinedRanker):
    def __init__(self, rankers, weights):
        super(LogLinearRanker, self).__init__(rankers)
        self.weights = weights if weights != None else numpy.ones(self.num_scores + 1)

    def combinator(self, mention, candidate, doc, scores):
        return numpy.dot(self.weights, scores + [1.])

class OnlineLogLinearRanker(LogLinearRanker):
    def __init__(self, rankers, learning_rate = 0.01):
        super(OnlineLogLinearRanker, self).__init__(rankers, None)
        self.alpha = learning_rate

    def save_parameters(self, path):
        with open(path, 'wb') as f:
            numpy.save(f, self.weights)

    def _score(self, mention, doc):
        assert hasattr(doc, 'gold_links_by_position'), 'doc must have gold links'
        mention_pos = (mention.begin, mention.end)
        gold_link = doc.gold_links_by_position.get(mention_pos, None)
        if gold_link != None:
            if gold_link in mention.candidates:
                mention.gold_candidate = gold_link
            else:
                #todo: we should eliminate these from the eval
                #log.debug('Gold link not part of the candidate set...')
                mention.gold_candidate = None
        
        return super(OnlineLogLinearRanker, self)._score(mention, doc)

    def combinator(self, mention, candidate, doc, scores):
        x = numpy.array(scores + [1.])
        y = super(OnlineLogLinearRanker, self).combinator(mention, candidate, doc, scores)

        # weight update only happens once per link
        if candidate == mention.gold_candidate:
            self.weights += (1. - y)*x*self.alpha
        else:
            pass
            #discount_factor = len(mention.candidates)
            #if mention.gold_candidate != None:
            #    discount_factor -= 1

            #self.weights += (0. - y)*x*(self.alpha / discount_factor)

        return y