import numpy
from sklearn.svm import LinearSVC
import cPickle as pickle
import logging
import random
import math

log = logging.getLogger()

class Train(object):
    """ Train classifier over a set of documents. """
    def __init__(self, doc_path, feature, classifier_path):
        self.in_path = doc_path
        self.out_path = classifier_path
        self.feature_set = [pickle.load(open(f, 'rb')) for f in feature]

    def iter_docs(self):
        with open(self.in_path, 'rb') as f:
            while True:
                try:
                    yield pickle.load(f)
                except EOFError: break

    @staticmethod
    def get_feature_vector(feature_set, doc, mention, candidate, kernel):
        if kernel == 'linear':
            return numpy.array([f[doc.id][(mention.begin, mention.end)][candidate] for f in feature_set])
        elif kernel == 'poly':
            # polynomial feature mapping
            fv = [math.sqrt(2.0) * f[doc.id][(mention.begin, mention.end)][candidate] for f in feature_set]
            sz = len(fv)
            for i in xrange(0, sz):
                for j in xrange(i, sz):
                    weight = 1.0 if i != j else math.sqrt(2.0)
                    fv.append(weight * fv[i]*fv[j])
            fv.append(1.0)

            return numpy.array(fv)
    
    @staticmethod
    def train(docs, feature_set, train_instance_limit = None, kernel='poly', C=0.0316228, penalty='l2', loss='l1', instance_selection='mag', instance_limit=10):
        X = []
        y = []

        instance_count = 0

        label = True
        for doc in docs:
            for m in doc.mentions:
                gold_link = doc.gold_links_by_position.get((m.begin, m.end), None)
                
                if gold_link in m.candidates:
                    instance_count += 1

                    gold_features = Train.get_feature_vector(feature_set, doc, m, gold_link, kernel)
                    negative_samples = [(c, Train.get_feature_vector(feature_set, doc, m, c, kernel)) for c in m.candidates if c != gold_link]

                    if instance_selection == 'mag_diff':
                        negative_samples = sorted(negative_samples, key=lambda (c, fv): numpy.abs(gold_features - fv).sum(), reverse=True)[:instance_limit]
                    elif instance_selection == 'random':
                        # learn from random subset of candidates
                        random.shuffle(negative_samples)
                        negative_samples = negative_samples[:instance_limit]
                    elif instance_selection == 'std':
                        # diversity in feature activation
                        negative_samples = sorted(negative_samples, key=lambda (c, fv): numpy.std(fv), reverse=True)[:instance_limit]
                    elif instance_selection == 'mag':
                        # learn from candidates with highest feature vector magnitude
                        # given feature vectors are standardised to have 0 mean and unit standard deviation, this  
                        # should be like selecting instances with the strongest feature activation
                        negative_samples = sorted(negative_samples, key=lambda (c, fv): numpy.abs(fv).sum(), reverse=True)[:instance_limit]

                    for c, candidate_features in negative_samples:
                        # class is toggled to balance training set for classifier
                        if label:
                            X.append(gold_features - candidate_features)
                            y.append(1.0)
                        else:
                            X.append(candidate_features - gold_features)
                            y.append(-1.0)

                        label = not label

                    # early stopping
                    if train_instance_limit != None and instance_count >= train_instance_limit: break
            if train_instance_limit != None and instance_count >= train_instance_limit: break

        log.info('Fitting model... (%i instances)' % (len(y)))

        dual = penalty == 'l2' and loss == 'l1'

        model = LinearSVC(dual=dual, penalty=penalty, C=C, loss=loss)
        model.fit(X, y)

        return model

    def __call__(self):
        """ Train classifier over documents """
        log.info('Preparing training set...')
        
        model = self.train(self.iter_docs(), self.feature_set)

        log.info('Saving classifier: %s' % self.out_path)
        pickle.dump(model, open(self.out_path, 'wb'))

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('doc_path', metavar='DOC_PATH')
        p.add_argument('--feature', metavar='FEATURE_MODEL_PATHS', action='append')
        p.add_argument('classifier_path', metavar='CLASSIFIER_MODEL')
        p.set_defaults(cls=cls)
        return p