from collections import defaultdict
from itertools import chain

class trie(object):
    def __init__(self):
        self.Children = defaultdict(trie)
        self.Matches = set()

    def insert_many(self, sequence, entities):
        if len(entities) > 0:
            self._insert(sequence, entities, 0, True)

    def insert(self, sequence, e):
        self._insert(sequence, e, 0, False)

    def _insert(self, sequence, e, offset, multi):
        if offset < len(sequence):
            item = sequence[offset]

            self.Children[item]._insert(sequence, e, offset + 1, multi)
        else:
            if multi:
                for entity in e:
                    self.Matches.add((entity, offset))
            else:
                self.Matches.add((e, offset))

    def iter_matches(self):
        for e in self.Matches: yield e

    def scan(self, seq):
        for i in xrange(0, len(seq)):
            for m in self.match(seq, i, True, True):
                yield m

    def match(self, seq, offset = 0, subsequences = False, inorder = True):
        # if we are yielding subsequence matches, or the sequence
        # is complete return all entities for the current node
        current = [(e, (offset - length, offset)) for e, length in self.iter_matches()] if subsequences or offset == len(seq) else None

        # iteration for the next items in the sequence
        pending = None
        if seq and offset < len(seq):
            token = seq[offset]
            if token in self.Children:
                pending = self.Children[token].match(seq, offset + 1, subsequences, inorder)

        if current and pending:
            return chain(current, pending) if inorder else chain(pending, current)
        
        return current or pending or []
