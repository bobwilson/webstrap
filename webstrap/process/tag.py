#!/usr/bin/env python
from .process import Process
from ..doc import Annotation, MentionChain
from ..model.model import Name
from ..process.trie import trie
from ..util import group

import os
import cPickle as pickle
import logging

log = logging.getLogger()

MAX_MENTION_LEN = 4

class Tagger(Process):
    """Mention tagger for tokenised text."""
    def __call__(self, doc):
        assert hasattr(doc, 'tokens'), 'doc must have tokens'
        doc.mentions = list(self._tag(doc))
        return doc

    def _tag(self, doc):
        """Yield mention annotations."""
        raise NotImplementedError

    def _mention_over_tokens(self, doc, i, j):
        """Create mention annotation from token i to token j-1."""
        toks = doc.tokens[i:j]
        begin = toks[0].begin
        end = toks[-1].end
        text = doc.text[begin:end]

        annotation = Annotation(begin, text)
        annotation.doc_token_span = (i, j)

        return annotation

class DummyTagger(Tagger):
    """Create one mention over full text."""
    def _tag(self, doc):
        """Yield mention annotations."""
        i = 0
        j = len(doc.tokens) - 1
        yield self._mention_over_tokens(doc, i, j)

class LookupTagger(Tagger):
    """
    Create mentions for names in an authority.
    Scan from left to right. Prefer longer mentions.
    """
    def __init__(self, model_path, lower=False, max_len=MAX_MENTION_LEN):
        self.name = Name(lower=lower)
        self.name.read(model_path)
        self.max_len = max_len

    def _tag(self, doc):
        """Yield mention annotations."""
        num_tokens = len(doc.tokens)
        i = 0
        while i < num_tokens:
            j = i + self.max_len
            while j > i:
                annot = self._mention_over_tokens(doc, i, j)
                cands = self._candidates(annot)
                if len(cands):
                    print annot # TESTING
                    print cands # TESTING
                    annot.candidates = cands
                    yield annot
                    i = j-1
                    break
                j -= 1
            i += 1

    def _candidates(self, mention):
        """Return candidates for mention."""
        norm = ' '.join(mention.text.split())
        return sorted(self.name.entities(norm), reverse=True)

class GoldTagger(Tagger):
    def __init__(self, entities_by_name, exclude_unlinkables = False):
        self.entities_by_name = entities_by_name
        self.exclude_unlinkables = exclude_unlinkables

    def _tag(self, doc):
        assert hasattr(doc, 'gold_links_by_position'), 'gold mention tagger requires doc with gold links'

        text_begin_to_tok_begin = {t.begin:i for i, t in enumerate(doc.tokens)}
        text_end_to_tok_end = {t.end:i for i, t in enumerate(doc.tokens)}

        doc_text = doc.text.lower()

        for (start, end), _ in doc.gold_links_by_position.iteritems():
            mention_text = doc_text[start:end]

            if mention_text in self.entities_by_name:
                # todo output statistics on token align mismatched
                if start in text_begin_to_tok_begin and end in text_end_to_tok_end:
                    mention = self._mention_over_tokens(
                        doc,
                        text_begin_to_tok_begin[start],
                        text_end_to_tok_end[end]+1)
                    mention.candidates = self.entities_by_name[mention_text]

                    gold_link = doc.gold_links_by_position.get((mention.begin, mention.end), None)
                    if gold_link not in mention.candidates:
                        log.warn('Entity (%s) not in candidate set for (%s)' % (gold_link, mention_text))
                        #log.debug('\n' + '\n\t'.join(mention.candidates))

                    if self.exclude_unlinkables and gold_link not in mention.candidates:
                        #mention.candidates.append(gold_link) # inject gold candidate
                        log.warn('Ignoring mention (%s), gold link (%s) not in the candidate set...' % (mention.text, gold_link))
                    else:
                        yield mention
                else:
                    log.warn('Token mismatch (%s): %s' % (doc.id, mention_text))
            else:
                log.warn('Missing alias (%s): %s' % (doc.id, mention_text))

class ExactMatchTagger(Tagger):
    def __init__(self, entities_by_name):
        # todo: generalise to any (name -> entity) set
        log.debug('Building trie of entity surface forms...')

        self.entity_trie = trie()

        for name, entities in entities_by_name.iteritems():
            tokenisation = name.lower().split()
            if tokenisation:
                self.entity_trie.insert_many(tokenisation, entities)

    def _tag(self, doc):
        """Yield mention annotations."""
        tokens = [t.text.lower() for t in doc.tokens]

        log.debug('Match tagging: ' + '-'.join(tokens[:5]) + '...')

        iter_mentions = self.entity_trie.scan(tokens)
        entitiesByMention = group(iter_mentions, lambda (_, p): p, lambda (e, _): e)

        for (start, end), entities in entitiesByMention.iteritems():
            mention = self._mention_over_tokens(doc, start, end)
            mention.candidates = entities
            yield mention

class StanfordTagger(Tagger):
    def __init__(self, tools_path, name_model_path):
        log.info("Initialising stanford entity tagger...")

        from nltk.tag.stanford import NERTagger
        self.tagger = NERTagger(
            os.path.join(tools_path, "classifiers/english.all.3class.distsim.crf.ser.gz"),
            os.path.join(tools_path, "stanford-ner.jar"))

        n = Name(lower=True)
        n.read(name_model_path)
        self.entities_by_name = {k:v.keys() for k, v in n.d.iteritems()}

    def to_mention(self, doc, start, end):
        mention = self._mention_over_tokens(doc, start, end)
        mention.candidates = self.entities_by_name.get(mention.text, [])
        log.debug('MENTION[%i,%i]: %s', start, end, mention.text)
        return mention

    def _tag(self, doc):
        i = 0
        start = None
        last = 'O'
        log.debug([t.text for t in doc.tokens])
        for i, (txt, tag) in enumerate(self.tagger.tag(t.text for t in doc.tokens)):
            log.debug('%i - %s - %s', i, txt, tag)
            if tag != last:
                if last != 'O':
                    yield self.to_mention(doc, start, i)
                last = tag
                start = i
            i += 1

        if last != 'O':
            yield self.to_mention(doc, start, i)

class CoreferenceTagger(object):
    """ Tags coreference chains in a document """
    def __init__(self, doc_model_path):
        self.input_path = doc_model_path

    def iter_docs(self):
        with open(self.input_path,'rb') as f:
            while True:
                try:
                    yield pickle.load(f)
                except EOFError:
                    break

    def tag(self, doc):
        doc.chains = []
        unchained_mentions = sorted(doc.mentions, key=lambda m: m.begin, reverse=True)

        #log.debug('MENTIONS: ' + ';'.join(m.text for m in unchained_mentions))
        while unchained_mentions:
            mention = unchained_mentions.pop(0)

            potential_antecedents = [(m.text, m) for m in unchained_mentions]
            chain = [mention]

            likely_acronym = False

            if mention.text.upper() == mention.text:
                # check if our mention is an acronym of a previous mention
                for a, m in potential_antecedents:
                    if ''.join(p[0] for p in a.split(' ')).upper() == mention.text:
                        chain.insert(0, m)
                        unchained_mentions.remove(m)
                        likely_acronym = True

            # check if we are a prefix/suffix of a preceding mention
            n = mention.text.lower()
            for a, m in potential_antecedents:
                na = a.lower()
                if (likely_acronym and mention.text == a) or \
                   (not likely_acronym and (na.startswith(n) or na.endswith(n) or n.startswith(na) or n.endswith(na))):
                    chain.insert(0, m)
                    unchained_mentions.remove(m)

            doc.chains.append(MentionChain(chain))
            #log.debug('CHAIN(%i): %s' % (len(potential_antecedents), ';'.join(m.text for m in chain)))

        return doc

    def __call__(self):
        for doc in self.iter_docs():
            self.tag(doc)


    @classmethod
    def add_arguments(cls, p):
        p.add_argument('doc_model_path', metavar='DOC_MODEL')
        p.set_defaults(cls=cls)
        return p