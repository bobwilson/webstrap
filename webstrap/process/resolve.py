#!/usr/bin/env python
from ..doc import Annotation
from .process import Process
from ..util import group, spanset_insert
from collections import OrderedDict

import logging
log = logging.getLogger()

class GoldResolver(Process):
    def __call__(self, doc):
        assert hasattr(doc, 'gold_links_by_position'), 'doc must have gold annotations'
        doc.links = list(self._resolve(doc))
        return doc

    def _resolve(self, doc):
        for m in doc.mentions:
            link = self._link(doc, m)
            if link is not None:
                yield link

    def _link(self, doc, mention):
        gold_link = doc.gold_links_by_position.get((mention.begin, mention.end), None)

        if gold_link != None:
            try:
                #gold_idx = [e.encode('utf-8') for e in mention.candidates].index(gold_link)
                #entity = mention.candidates[gold_idx]
                link = mention.copy()
                link.resolution = (1.0, gold_link.encode('utf-8'))
                return link
            except ValueError: pass

class ThresholdResolver(Process):
    def __init__(self, threshold):
        self.threshold = threshold
    
    def __call__(self, doc):
        assert hasattr(doc, 'mentions'), 'doc must have mentions'
        doc.links = list(self._resolve(doc))
        return doc

    def _resolve(self, doc):
        for m in doc.mentions:
            link = self._link(m)
            if link is not None:
                yield link

    def _link(self, mention):
        if len(mention.candidates) > 0:
            score, entity = mention.candidates[0]
            if self.threshold == None or score >= self.threshold:
                link = mention.copy()
                link.resolution = (score, entity)
                return link

class GreedyOverlapResolver(Process):
    def __call__(self, doc):
        assert hasattr(doc, 'links'), 'doc must have links'
        doc.links = list(self._resolve(doc.links))
        return doc

    @staticmethod
    def _resolve(links):
        """ Resolve overlaps by taking the longest mention span available, breaking ties by link score """

        # group links by the length of their mention
        links_by_length = group(links, lambda l: l.end - l.begin, lambda l: l)

        # sort by mention length
        links_by_length = OrderedDict(sorted(links_by_length.items(), reverse = True))

        # holds the running set of disjoint mention spans in the document
        span_indicies = []

        for _, lks in links_by_length.items():
            # for all links of a given length, insert them in order of score
            for link in sorted(lks, key = lambda l: l.resolution, reverse = True):
                # only resolve this link if it's mention span doesn't overlap with a previous insert
                if spanset_insert(span_indicies, link.begin, link.end - 1):
                    yield link

class ResolutionEntityFilter(Process):
    """Maps entity ids"""
    def __init__(self, predicate):
        self.predicate = predicate

    def __call__(self, doc):
        doc.links = [l for l in doc.links if self.predicate(l.resolution[1])]
        return doc

class ResolutionEntityMapper(Process):
    """Maps entity ids"""
    def __init__(self, mapper):
        self.mapper = mapper

    def __call__(self, doc):
        for link in doc.links:
            score, entity = link.resolution
            link.resolution = score, self.mapper(entity)
        return doc