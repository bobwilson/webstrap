#!/usr/bin/env bash
#
# Fetch docrep files from Glen across cluster

usage="Usage: $0 QUERIES OUTDIR DRDIR"

if [ "$#" -ne 3 ]; then
    echo $usage
    exit 1
fi

queries=$1; shift # path to TAC12 linking queries file
outdir=$1; shift # output directory
drdir=$1; shift # directory containing docrep files

SCHWASF=/n/schwafs/home/bhachey/repos/schwa/slotfilling/sf

read -d '' cmd <<EOF
cd $PWD;
. ve/bin/activate;
export PYTHONPATH=$SCHWASF;
nice ./webstrap/utility/tac_dr_fetch.sh $queries $outdir $drdir
EOF

for n in 06 07 08 09 10 11
do
		ssh -f schwa${n}.it.usyd.edu.au "$cmd"
done
