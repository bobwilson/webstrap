#!/usr/bin/env bash
#
# Fetch docrep files from Glen
#
# Add schwa.slotfilling.sf to PYTHONPATH before running, e.g.:
#   export PYTHONPATH=/n/schwafs/home/bhachey/repos/schwa/slotfilling/sf/

usage="Usage: $0 QUERIES OUTDIR DRFILE"

if [ "$#" -ne 3 ]; then
    echo $usage
    exit 1
fi

queries=$1; shift # path to TAC12 linking queries file
outdir=$1; shift # output directory
drdir=$1; shift # directory containing docrep files

ls $drdir/*.dr \
		| xargs -P16 -n1 python webstrap/utility/tac_dr_fetch.py $queries $outdir
