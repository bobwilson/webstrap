#!/usr/bin/env python -W ignore::DeprecationWarning
"""
Corpus model and script to fetch docrep files from Glen

Before running fetch script:
1 - Clone https://github.com/schwa-lab/slotfilling/
2 - Add $slotfilling/sf to $PYTHONPATH

Dependencies:
* libschwa-python
"""
from schwa import dr
from xml.etree.cElementTree import iterparse
import os

import logging

log = logging.getLogger()

# query xml element and attribute names
QUERY_ELEM = 'query'
QID_ATTR   = 'id'
DOCID_ELEM = 'docid'
START_ELEM = 'beg'
END_ELEM   = 'end'
NAME_ELEM  = 'name'
DEFAULT_OFFSET = 1
ENC = 'utf8'

tokens_on_entities = dr.decorators.materialize_slices('entities', 'tokens', 'span', 'tokens')

@dr.requires_decoration(tokens_on_entities)
def decorate(doc):
    return doc

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    class Token(dr.Ann):
        raw = dr.Text(help='Raw string form')
        norm = dr.Text(help='Norm string form')
        pos = dr.Field()
        lemma = dr.Field()
        span = dr.Slice()
        tidx = dr.Field()

        @property
        def text(self):
            return self.norm or self.raw

    class Entity(dr.Ann):
        span = dr.Slice(Token)
        label = dr.Field(help='NE or Topic type label')
      
        @property
        def text(self):
            return ' '.join(t.text for t in self.tokens)

    class DrDoc(dr.Doc):
        doc_id = dr.Field()
        tokens = dr.Store(Token)
        entities = dr.Store(Entity)

class Query(object):
    def __init__(self, id, doc_id, start, end, name):
        assert isinstance(name, unicode)
        self.id = id
        self.doc_id = doc_id
        self.start = start
        self.end = end
        self.name = name

    def __str__(self):
        return u'Query<{}: {}>'.format(
            self.id,
            self.name
            ).encode(ENC)

class QueryReader(object):
    def __init__(self, queries_file, offset=DEFAULT_OFFSET):
        """
        queries_file - file containing query set
        offset - integer to add to end offset (0 for <=2011, 1 for >=2012)
        """
        self.queries_file = queries_file
        self.offset = offset

    def __iter__(self):
        "Yield (qid, docid, start, end, name) tuples"
        for event, elem in iterparse(self.queries_file):
            if elem.tag == QUERY_ELEM:
                yield self._query(elem)

    def _query(self, query_elem):
        "Return (qid, docid, start, end, name) tuple"
        query_id = query_elem.get(QID_ATTR)
        d = {}
        for child in query_elem:
            d[child.tag] = child.text
        doc_id = d[DOCID_ELEM]
        try:
            start = int(d[START_ELEM])
            end = int(d[END_ELEM]) + self.offset
        except KeyError:
            start = None
            end = None
        name = unicode(d[NAME_ELEM])
        return Query(query_id, doc_id, start, end, name)


class DrWriter(object):
    def __init__(self, outdir, doc_cls=DrDoc):
        """
        outdir - output directory
        doc_cls - docrep document class
        """
        self.outdir = outdir
        self.doc_cls = doc_cls

    def write(self, doc):
        "Write doc to outdir with doc_id as filename"
        path = os.path.join(self.outdir, '{}.dr'.format(doc.doc_id))
        fh = open(path, 'wb')
        dr.Writer(fh, self.doc_cls).write(doc)


class DrReader(object):
    def __init__(self, drfile, doc_cls=DrDoc, decorate_fn=decorate):
        """
        drfile - path to a docrep collection file
        doc_cls - docrep document class
        decorate_fn - docrep decorator function
        """
        self.drfile = drfile
        self.doc_cls = doc_cls
        self.decorate_fn = decorate_fn

    def __iter__(self):
        "Yield docrep objects"
        docs = dr.Reader(open(self.drfile, "rb"), self.doc_cls)
        for doc in docs:
            self.decorate_fn(doc)
            yield doc

def fetch(drfile, doc_list_path, outdir):
    """
    Grab preprocessed documents
    drfile - path to a docrep collection file')
    queries - path to TAC12 linking queries file
    outdir - output directory
    """
    with open(doc_list_path, 'r') as f:
        docids = frozenset(doc_id.strip() for doc_id in f)

    w = DrWriter(outdir, DrDoc)
    for doc in DrReader(drfile, DrDoc, decorate):
        if doc.doc_id in docids:
            w.write(doc)

# internal processing for glens doc (pull tac query docs from dr batches)
if __name__ == '__main__':
    import argparse
    p = argparse.ArgumentParser(description='Fetch TAC12 query docs')
    p.add_argument('doc_list_path', help='Path to file containing list of document ids')
    p.add_argument('outdir', help='Output directory')
    p.add_argument('drfile', help='Path to a docrep collection file')
    args = p.parse_args()
    fetch(args.drfile, args.doc_list_path, args.outdir)
