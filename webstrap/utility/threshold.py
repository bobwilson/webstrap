#!/usr/bin/env python
"""
Set a threshold based on pickled documents and model

Call as, e.g.:
  python webstrap/utility/threshold.py \
    /data0/linking/data/tac/2014/prepared/train_h1.doc.model \
    /data0/linking/data/tac/2014/features/classifiers/train_h1.0b62e88b67c833b552f31ed97ac3fe2c.classifier.feature.model
"""
import cPickle as pickle
import operator

def read_docs(path):
    fh = open(path)
    while True:
        try:                                                                                 yield pickle.load(fh)
        except EOFError:
            break

def read_model(path):
    return pickle.load(open(path))

def collate_all(model, doc):
    for m in doc.mentions:
        span = (m.begin, m.end)
        gold = doc.gold_links_by_position.get(span)
        if gold is None:
            continue
        scores = model.get(doc.id).get(span,[])
        for candidate, score in scores.iteritems():
            if candidate == gold:
                yield (doc.id, m.begin, m.text, candidate, score, 1)
            else:
                yield (doc.id, m.begin, m.text, candidate, score, 0)

def collate(model, doc, n=1):
    for m in doc.mentions:
        span = (m.begin, m.end)
        gold = doc.gold_links_by_position.get(span)
        if gold is None:
            continue
        scores = sorted(model.get(doc.id).get(span,{}).items(),
                        key=operator.itemgetter(1),
                        reverse=True)
        for candidate, score in scores[:n]:
            if candidate == gold:
                yield (doc.id, m.begin, m.text, candidate, score, 1)
            else:
                yield (doc.id, m.begin, m.text, candidate, score, 0)

def main(docs_path, model_path, num_candidates):
    model = read_model(model_path)
    for doc in read_docs(docs_path):
        for instance in collate(model, doc, num_candidates):
            print '\t'.join([unicode(f).encode('utf8') for f in instance])

if __name__ == '__main__':
    import argparse
    p = argparse.ArgumentParser(description='Set threshold')
    p.add_argument('docs', help='file containing document pickles')
    p.add_argument('model', help='file containing model pickle')
    p.add_argument('-n', type=int, default=1,
                   help='number of candidates per mention')
    args = p.parse_args()
    main(args.docs, args.model, args.n)

