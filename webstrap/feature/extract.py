#!/usr/bin/env python
import logging
import argparse
import textwrap
import cPickle as pickle
import numpy
from time import time

from ..util import parmapper
from .feature import FEATURE_SET

# not referenced explicitly but must be imported to register extractable features
from webstrap.feature import probability, context, meta, freebase # pylint: disable=I0011,W0611

log = logging.getLogger()

class ExtractFeature(object):
    "Extract features from a prepared document model."
    FEATURES = FEATURE_SET

    def __init__(self, **kwargs):
        self.in_path = kwargs.pop('doc_model_path')
        self.standardisation_model_path = kwargs.pop('standardisation_model_path')
        self.out_path = kwargs.pop('feature_model_path')
        self.feature = kwargs.pop('featurecls')(**kwargs)

    def iter_docs(self):
        with open(self.in_path, 'rb') as f:
            while True:
                try:
                    yield pickle.load(f)
                except EOFError: break

    def extract_document_features(self, doc):
        features = {} # mention.begin, mention.end -> candidate -> value
        for m in doc.mentions:
            features[(m.begin, m.end)] = {}
            for c in m.candidates:
                features[(m.begin, m.end)][c] = self.feature.compute(doc, m, c)
        return (doc.id, features)

    def extract_doc_features(self):
        # doc.id -> mention.begin, mention.end -> candidate -> value
        doc_features = {}

        # track performance statistics of the feature extraction process
        total_docs = 0
        total_mentions = 0
        total_candidates = 0
        start_time = time()

        with parmapper(self.extract_document_features, 30, recycle_interval=5) as pm:
            log.info("Starting parallel feature extraction...")
            for _, (doc_id, features) in pm.consume(self.iter_docs()):
                total_docs += 1
                total_mentions += len(features)
                total_candidates += sum(len(cs) for cs in features.itervalues())

                if total_docs % 200 == 0:
                    duration = float(time() - start_time)
                    perf_stats_str = '( %.2f d/s %.2f m/s %.2f c/s )' % (total_docs/duration, total_mentions/duration, total_candidates/duration)
                    log.info('Extracted feature for %i docs... %s' % (total_docs, perf_stats_str))

                doc_features[doc_id] = features

        # standardise features for zero mean and unit variance
        values = list(value for m in doc_features.itervalues() for c in m.itervalues() for value in c.itervalues())

        if self.standardisation_model_path == None:
            log.info('Calculating feature standardisation parameters...')
            mean = numpy.mean(values)
            std = numpy.std(values)
        else:
            log.info('Loading feature standardisation parameters...')
            mean, std = pickle.load(open(self.standardisation_model_path,'rb'))

        log.info('Feature mean: %.4f', mean)
        log.info('Feature std:  %.4f', std)
        for mentions in doc_features.itervalues():
            for candidates in mentions.itervalues():
                for candidate, value in candidates.iteritems():
                    candidates[candidate] = (value - mean) / std

        return doc_features, (mean, std)

    def __call__(self):
        """ Extract and save document features """
        features, standardisation_params = self.extract_doc_features()
        log.info('Saving feature model: %s ...' % self.out_path)
        pickle.dump(features, open(self.out_path, 'wb'))
        pickle.dump(standardisation_params, open(self.out_path + '.standardisation.model', 'wb'))
        log.info('Feature extraction complete')

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('doc_model_path', metavar='DOC_MODEL')
        p.add_argument('--standardisation_model_path', default=None, required=False, metavar='STANDARDISATION_MODEL')
        p.add_argument('feature_model_path', metavar='FEATURE_MODEL')
        p.set_defaults(cls=cls)

        sp = p.add_subparsers()
        for featurecls in cls.FEATURES:
            name = featurecls.__name__
            help_str = featurecls.__doc__.split('\n')[0]
            desc = textwrap.dedent(featurecls.__doc__.rstrip())
            csp = sp.add_parser(name,
                                help=help_str,
                                description=desc,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
            featurecls.add_arguments(csp)

        return p

