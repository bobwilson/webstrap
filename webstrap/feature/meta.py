#!/usr/bin/env python
from .feature import Feature
from webstrap.model.model import EntityOccurrence
from collections import defaultdict
from functools32 import lru_cache
from scipy.sparse import csc_matrix
import operator
import cPickle as pickle
import numpy as np
import math
import logging

log = logging.getLogger()

@Feature.Extractable
class ClassifierScore(Feature):
    """ Computes a feature score based on the output of a classifier over a set of features. """
    def __init__(self, classifier_model_path, feature, kernel='poly'):
        self.kernel = kernel

        log.debug('Loading classifier...')
        self.classifier = pickle.load(open(classifier_model_path, 'rb'))

        log.debug('Loading features...')
        self.feature_set = [pickle.load(open(f, 'rb')) for f in feature]

    def get_feature_vector(self, doc, mention, candidate):
        if self.kernel == 'linear':
            return np.array([f[doc.id][(mention.begin, mention.end)][candidate] for f in self.feature_set])
        elif self.kernel == 'poly':
            fv = [math.sqrt(2.0) * f[doc.id][(mention.begin, mention.end)][candidate] for f in self.feature_set]
            sz = len(fv)
            for i in xrange(0, sz):
                for j in xrange(i, sz):
                    weight = 1.0 if i != j else math.sqrt(2.0)
                    fv.append(weight * fv[i]*fv[j])
            fv.append(1.0)
            return np.array(fv)

    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
        return np.dot(self.get_feature_vector(doc, mention, candidate), self.classifier.coef_.T)[0]

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('classifier_model_path', metavar='CLASSIFIER_MODEL')
        p.add_argument('--feature', metavar='FEATURE_MODEL_PATHS', action='append')
        p.set_defaults(featurecls=cls)
        return p

class RelatednessBase(Feature):
    """ Computes a relatedness feature score based on coherence between entities in the candidate set."""
    def __init__(self, cooccurence_model_path, score_model_path):
        self.cooccurence_model = EntityOccurrence.read(cooccurence_model_path)

        log.debug('Loading features...')
        self.score_model = pickle.load(open(score_model_path, 'rb'))

    def get_candidate_score(self, doc, mention, candidate):
        return self.score_model[doc.id][(mention.begin, mention.end)][candidate]

    @lru_cache(maxsize=None)
    def _get_top_scores(self, doc_id, mention_begin, mention_end, n):
        top_scores = sorted(self.score_model[doc_id][(mention_begin, mention_end)].iteritems(), key=operator.itemgetter(1), reverse=True)[:n]
        return [(s/top_scores[0][1], c) for c, s in top_scores]
    
    def get_top_scores(self, doc, mention, n):
        return self._get_top_scores(doc.id, mention.begin, mention.end, n)

    def compute(self, doc, mention, candidate):
        raise NotImplementedError

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('cooccurence_model_path', metavar='COCCURRENCE_MODEL_PATH')
        p.add_argument('score_model_path', metavar='SCORE_MODEL_PATH')
        p.set_defaults(featurecls=cls)
        return p

class JaccardCoherence(RelatednessBase):
    """ Computes jaccard similarity between entities. """
    @lru_cache(maxsize=1000000)
    def jaccard_relatedness(self, a, b):
        # todo update
        occ_a = self.cooccurence_model.occurrences(a)
        occ_b = self.cooccurence_model.occurrences(b)
        num_common = len(occ_a.intersection(occ_b))
        num_union = len(occ_a.union(occ_b))

        try:
            return math.log(float(num_common) / float(num_union))
        except (ZeroDivisionError, ValueError):
            return -20.0

    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
    
        res = [self.get_top_scores(doc, m, 1)[0][1] for m in doc.mentions if m != mention and m.candidates]

        try:
            return sum(self.jaccard_relatedness(candidate if candidate >= e else e, e if candidate >= e else candidate) for e in res) / float(len(res))
        except ZeroDivisionError:
            return 0.0

@Feature.Extractable
class ConditionalCoherence(RelatednessBase):
    """ Computes coherence measure based on conditional probability of top ranked candidates. """
    @lru_cache(maxsize=None)
    def conditional_probability(self, a, b):
        # using occurrence model
        occ_a = self.cooccurence_model.occurrences(a)
        occ_b = self.cooccurence_model.occurrences(b)
        num_common = len(occ_a.intersection(occ_b))
        num_union = len(occ_b)
        try:
            return math.log(float(num_common) / float(num_union))
        except (ZeroDivisionError, ValueError):
            return -20.0

        # with cooccurrence model
        #p_a_b = self.cooccurence_model.conditional_probability(a, b)
        #if p_a_b != 0.0:
        #    return math.log(p_a_b)
        #else:
        #    return -20.0

    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
    
        res = [self.get_top_scores(doc, m, 1)[0][1] for m in doc.mentions if m != mention and m.candidates]

        try:
            return sum(self.conditional_probability(candidate, e) for e in res) / float(len(res))
        except ZeroDivisionError:
            return -20.0 * 30

class UnambiguousConditionalCoherence(ConditionalCoherence):
    """ Computes weighted conditional coherence across unambiguous candidates. """
    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
    
        res = [self.get_top_scores(doc, m, 1)[0][1] for m in doc.mentions if m != mention and len(m.candidates) == 1]

        try:
            return sum(self.conditional_probability(candidate, e) for e in res) / float(len(res))
        except ZeroDivisionError:
            return 0.0

class MaxConditionalCoherence(ConditionalCoherence):
    """ Computes weighted conditional coherence across the top candidates. """ 
    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
        score = -20.0
        for m in doc.mentions:
            if m != mention:
                score = max(score, max(self.conditional_probability(candidate, e) for _, e in self.get_top_scores(doc, m, 2)))
        return score

@Feature.Extractable
class MaxMultiConditionalCoherence(ConditionalCoherence):
    """ Computes weighted conditional coherence across the top candidates. """ 
    def compute(self, doc, mention, candidate):
        """ Returns a feature value """
        score = 0.0
        for m in doc.mentions:
            if m != mention:
                score += max(self.conditional_probability(candidate, e) for _, e in self.get_top_scores(doc, m, 2))
        return score
        
        # normalising this score over the number of mentions, i.e. returning the average log
        # condition probability does not perform as well as simply returning the sum.
        # this is kind of surprising given how the feature value works numerically
        # obvs multiple cofererent metions are good evidence, regardless of avg coherence
        #if len(doc.mentions) > 1:
        #    return score / float(len(doc.mentions) - 1)
        #else:
        #    return -20.0

class MultiConditionalCoherence(ConditionalCoherence):
    """ Computes weighted conditional coherence across the top candidates. """ 
    def compute(self, doc, mention, candidate):
        """ Returns a feature value """ 
        res = []
        for m in doc.mentions:
            if m != mention:
                scores = self.get_top_scores(doc, m, 3)

                total_score = sum(s for s, _ in scores)
                for s, c in scores:
                    res.append((s/total_score, c))

        try:
            return sum(s*self.conditional_probability(candidate, e) for s, e in res) / float(len(res))
        except ZeroDivisionError:
            return 0.0

@Feature.Extractable
class PageRankCoherence(ConditionalCoherence):
    """ Computes candidate coherence based on pagerank of candidates over the document mention-candidate graph """
    
    @staticmethod
    def pageRank(G, s = .85, maxerr = .001, maxiter=1000):
        """ https://gist.github.com/diogojc/1338222 """ 
        n = G.shape[0]

        # transform G into markov matrix M
        M = csc_matrix(G,dtype=np.float)
        rsums = np.array(M.sum(1))[:,0]
        ri, ci = M.nonzero()
        M.data /= rsums[ri]

        # bool array of sink states
        sink = rsums==0

        # Compute pagerank r until we converge
        ro, r = np.zeros(n), np.ones(n)

        iterations = 0 
        while iterations < maxiter and np.sum(np.abs(r-ro)) > maxerr:
            ro = r.copy()

            # calculate each pagerank at a time
            for i in xrange(0,n):
                # inlinks of state i
                Ii = np.array(M[:,i].todense())[:,0]
                # account for sink states
                Si = sink / float(n)
                # account for teleportation to state i
                Ti = np.ones(n) / float(n)

                r[i] = ro.dot( Ii*s + Si*s + Ti*(1-s) )

            iterations += 1

        return r

        # return normalized pagerank
        #return r/sum(r)       

    def _cp(self, a, b):
        occ_a = self.cooccurence_model.occurrences(a)
        occ_b = self.cooccurence_model.occurrences(b)
        num_common = len(occ_a.intersection(occ_b))
        num_union = len(occ_b)

        return 0.0 if num_union == 0 else float(num_common) / float(num_union)
    
    @lru_cache(maxsize=1000000)
    def ref(self, a, b):
        return b in self.cooccurence_model.occurrences(a) or a in self.cooccurence_model.occurrences(b)

    @lru_cache(maxsize=1000000)
    def num_common(self, a, b):
        return len(self.cooccurence_model.occurrences(a).intersection(self.cooccurence_model.occurrences(b)))

    def compute(self, doc, mention, candidate):
        # bit of a hack to ensure we only compute pageranks once per document
        # despite features being called for on a per-candidate basis
        if not hasattr(doc, 'page_ranks'):
            doc.page_ranks = defaultdict(lambda: defaultdict(float))
            
            mention_candidates = [(m, c) for m in doc.mentions for c in self.get_top_scores(doc, m, 3)]
            n = len(mention_candidates)

            #log.debug('[%s] Computing pagerank over %i mention-candidates...' % (doc.id, n))
            #start_time = time()

            M = np.zeros((n, n))
            for i, (mi, (si, ci)) in enumerate(mention_candidates):
                for j, (mj, (sj, cj)) in enumerate(mention_candidates):
                    if i != j and mi.text.lower() != mj.text.lower():
                        M[i,j] = 20.0 + self.conditional_probability(ci, cj)
                        #M[i,j] = 1.0 if self.ref(ci, cj) or self.num_common(ci, cj) > 0 else 0.0
            
            # normalisation of the link-weight matrix?
            c_sums = M.sum(axis=0)
            c_sums[c_sums==0] = 1. # ensure we don't introduce NaN's for all-zero columns
            M = M / c_sums

            ranks = self.pageRank(M, s = .85, maxerr=0.01)
            
            #log.debug('[%s] Completed %i mention-candidates in %i seconds...' % (doc.id, n, int(time() - start_time)))
            
            for i, (m, (s, c)) in enumerate(mention_candidates):
                pr = ranks[i]
                # todo: normalisation of pagerank scores amungst a mention's candidate set
                #       if we don't do this, candidate scores may be diluted by the size of the mention-candidate graph
                #       alternatively, we could remove normalisation from the end of the pagerank calculation above

                log.debug('%s: S = %.4f, PR = %.4f' % (c, s, pr))
                doc.page_ranks[m][c] = pr

        return doc.page_ranks[mention][candidate]

