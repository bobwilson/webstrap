#!/usr/bin/env python
from .feature import Feature
from functools32 import lru_cache
from webstrap.model.model import Name, Entity
from ..process.tag import CoreferenceTagger
import math
import logging

log = logging.getLogger()

class NormalisedLogProbability(Feature):
    def candidate_probability(self, doc, mention, candidate):
        """ Returns a dict of candidate->probability """
        raise NotImplementedError
    
    @lru_cache(maxsize=1)
    def get_max_log_p(self, doc, mention):
        return math.log(min(self.candidate_probability(doc, mention, c) for c in mention.candidates))

    def compute(self, doc, mention, candidate):
        """ Returns a feature value """

        #max_log_p = self.get_max_log_p(doc, mention)
        candidate_p = self.candidate_probability(doc, mention, candidate)
        
        return math.log(candidate_p)

        # normalise log probability amungst the candidate set
        #return 0.0 if max_log_p == 0.0 else 1.0 - (math.log(candidate_p) / max_log_p)

@Feature.Extractable
class NameProbability(NormalisedLogProbability):
    """ Name probability. """
    def __init__(self, name_model_path):
        # todo: parameterise mention normalisiation
        self.name_model = Name(lower = True)
        self.name_model.read(name_model_path)

    def candidate_probability(self, doc, mention, candidate):
        # todo: parameterise mention normalisiation
        return self.name_model.score(mention.text.lower(), candidate, [c.lower() for c in mention.candidates])

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('name_model_path', metavar='NAME_MODEL')
        p.set_defaults(featurecls=cls)
        return p

@Feature.Extractable
class CorefNameProbability(NormalisedLogProbability):
    """ Name probability. """
    def __init__(self, name_model_path):
        # todo: parameterise mention normalisiation
        self.name_model = Name(lower = True)
        self.name_model.read(name_model_path)

        self.coref_tagger = CoreferenceTagger(None)

    def candidate_probability(self, doc, mention, candidate):
        if not hasattr(mention, 'chain'):
            self.coref_tagger.tag(doc)

        mentions_by_length = sorted([m.text.lower() for m in mention.chain.mentions], key=len, reverse=True)
        coreferent_name = mentions_by_length[0]
        for n in sorted([m.text.lower() for m in mention.chain.mentions], key=len, reverse=True):
            if n in self.name_model.d:
                coreferent_name = n
                break

        # todo backoff if P(e|n) for coref'd name not found?
        return self.name_model.score(coreferent_name, candidate, [c.lower() for c in mention.candidates])

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('name_model_path', metavar='NAME_MODEL')
        p.set_defaults(featurecls=cls)
        return p

@Feature.Extractable
class NameGivenEntityProbability(NormalisedLogProbability):
    """ Probability of the mentioned name given the candidate entity. """
    def __init__(self, name_given_entity_model_path):
        # todo: parameterise mention normalisiation
        self.name_given_entity_model = Name(lower = True)
        self.name_given_entity_model.read(name_given_entity_model_path)

    def candidate_probability(self, doc, mention, candidate):
        # todo: parameterise mention normalisiation
        return self.name_given_entity_model.score(candidate, mention.text.lower())

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('name_given_entity_model_path', metavar='NAME_COND_ENTITY_MODEL')
        p.set_defaults(featurecls=cls)
        return p

@Feature.Extractable
class EntityProbability(NormalisedLogProbability):
    """ Entity probability. """
    def __init__(self, entity_model_path):
        self.entity_model = Entity()
        self.entity_model.read(entity_model_path)

    def candidate_probability(self, doc, mention, candidate):
        return self.entity_model.score(candidate)

    @classmethod
    def add_arguments(cls, p):
        p.add_argument('entity_model_path', metavar='ENTITY_MODEL')
        p.set_defaults(featurecls=cls)
        return p