#!/usr/bin/env python
from ..process.process import Process

FEATURE_SET = set()

class Feature(object):
    @staticmethod
    def Extractable(c):
        FEATURE_SET.add(c)
        return c

    def compute(self, doc, mention, candidate):
        """ Returns value of the feature for the candidate """
        raise NotImplementedError

class FeatureExtractor(Process):
    def __init__(self):
        pass

    def __call__(self, doc):
        assert hasattr(doc, 'mentions'), 'doc must have mentions'
        for m in doc.mentions:
            for c in m.candidates:
                c.features = self.candidate_features(doc, m, c)
        return doc

    def candidate_features(self, doc, mention, candidate):
        """ Returns a [feature vector] """
        raise NotImplementedError

class FeatureSetExtractor(FeatureExtractor):
    def __init__(self, features):
        self.features = features

    def candidate_features(self, doc, mention, candidate):
        """ Returns a [feature vector] """
        return [f.compute(doc, mention, candidate) for f in self.features]

class OfflineFeatureSetExtractor(FeatureExtractor):
    def __init__(self, feature_models, feature_set):
        self.feature_models = feature_models
        self.feature_set = feature_set

    def candidate_features(self, doc, mention, candidate):
        return [self.feature_models[f][doc.id][(mention.begin, mention.end)][candidate] for f in self.feature_set]