import os
import code
import marshal
import subprocess
import operator
import sys
from webstrap.model.model import mmdict, Redirects, EntityMentionContexts
from collections import defaultdict, Counter, OrderedDict
import numpy

model_path = '/n/schwa11/data0/linking/models'
conll_data_path = '/n/schwa11/data0/linking/data/conll/'
redirect_model = marshal.load(open(os.path.join(model_path, 'wikipedia.redirect.model'), 'rb'))

print 'Preparing candidate model...'
alias_model = marshal.load(open(os.path.join(model_path, 'aida_yago_means.alias.model'), 'rb'))
candidate_model = {}
for entity, names in alias_model.iteritems():
    for name in (n.lower() for n in names):
        candidate_model.setdefault(name, set()).add(entity)

wikilinks_occurrence_model_path = os.path.join(model_path,'wikilinks.cooccurrence.model')
wikipedia_occurrence_model_path = os.path.join(model_path,'wikipedia.cooccurrence.model')

wikilinks_name_count_model_path = os.path.join(model_path,'wikilinks.name_count.model')
wikipedia_name_model_path = os.path.join(model_path,'dbpedia.name.model')

analysis_doc = 'testa'
test_path = os.path.join(conll_data_path, 'gold_%s.txt' % analysis_doc)

wikilinks_mention_path = os.path.join(model_path, 'wikilinks.entity_context.w_none.tf.model')
kopiwiki_article_path = os.path.join(model_path, 'kopiwiki_article.entity_context.w_none.tf.model')
kopiwiki_mention_path = os.path.join(model_path, 'kopiwiki_mention.entity_context.w_none.tf.model')

wikilinks_mentions_path = os.path.join(model_path, 'wikilinks.mention.model')

# context = 2c49de5475610960cfdb7c29e88ba37b
# full = c56f56b8d4a5c94a5a54ef7063d029ee
kopiwiki_mention_analysis_path = 'output/conll/testa.2c49de5475610960cfdb7c29e88ba37b.analysis' # context
#kopiwiki_mention_analysis_path = 'output/conll/testa.c56f56b8d4a5c94a5a54ef7063d029ee.analysis' # full

# full = 0d416ec72d3667084363427110593a8b
# context = 7064e9e782e510ed26a12de508f43d76
kopiwiki_article_analysis_path = 'output/conll/testa.7064e9e782e510ed26a12de508f43d76.analysis'
#kopiwiki_article_analysis_path = 'output/conll/testa.0d416ec72d3667084363427110593a8b.analysis'

# full = ffa8b45f2f10f737f3b3a721e3317e87
# context = 07d8a775ba88714da52edfe76b3b823f
wikilinks_mention_analysis_path = 'output/conll/testa.07d8a775ba88714da52edfe76b3b823f.analysis'
#wikilinks_mention_analysis_path = 'output/conll/testa.ffa8b45f2f10f737f3b3a721e3317e87.analysis'

feature_names = [
    'BoWMentionContext.kopiwiki_article',
    'BoWMentionContext.kopiwiki_mention',
    'BoWMentionContext.wikilinks',
    'DAvgBoWMentionContext.kopiwiki_article',
    'DAvgBoWMentionContext.kopiwiki_mention',
    'DAvgBoWMentionContext.wikilinks',
    'LLMDBoWMentionContext.wikilinks',
    'NameProbability.dbpedia',
    'NameProbability.wikilinks'
]

def iter_analysis_file_errors(path):
    with open(path, 'r') as f:
        for l in f:
            parts = l.split('\t')
            if parts[0] == 'wrong-link':
                mention, gold_entity, system_entity = tuple(p[2:-1].decode('utf-8') for p in parts[2:])
                gold_entity = redirect_model.get(gold_entity, gold_entity)
                system_entity = redirect_model.get(system_entity, system_entity)
                yield (mention, gold_entity, system_entity)

def to_sorted_dict(d):
    return OrderedDict(sorted(d.iteritems(), key=operator.itemgetter(1), reverse = True))

# array of gold links from testb: [(mention text, gold_entity)]
links = []
p = subprocess.Popen(['grep', '-P', '\\tB\\t', test_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
output, _ = p.communicate()
lines = output.split('\n')
for l in lines:
    parts = l.decode('utf-8').strip().split('\t')
    if len(parts) == 4:
        links.append((parts[2], redirect_model.get(parts[3],parts[3])))

entity_counts = Counter(e for _, e in links)
entity_counts = to_sorted_dict(entity_counts)

#print "\nMost common entities in Testb:"
#print entity_counts.items()[:5]
test_entities = set(entity_counts.iterkeys())


print "\nEvaluation entities / instances:"
print "\t{:s}\t{:,d}\t{:,d}".format(analysis_doc, len(test_entities), len(links))

WIKILINKS = "Wikilinks"
KOPI_ARTICLE = "Kopi Article"
KOPI_MENTION = "Kopi Mention"
context_model_paths = {
    KOPI_ARTICLE: kopiwiki_article_path,
    KOPI_MENTION: kopiwiki_mention_path,
    WIKILINKS:    wikilinks_mention_path
}
context_models = [
    (KOPI_ARTICLE,   mmdict(kopiwiki_article_path)),
    (KOPI_MENTION,   mmdict(kopiwiki_mention_path)),
    (WIKILINKS,      mmdict(wikilinks_mention_path))
]
analysis_errors = [
    (KOPI_ARTICLE,   list(iter_analysis_file_errors(kopiwiki_article_analysis_path))),
    (KOPI_MENTION,   list(iter_analysis_file_errors(kopiwiki_mention_analysis_path))),
    (WIKILINKS,      list(iter_analysis_file_errors(wikilinks_mention_analysis_path)))
]

def format_source_stats(svs):
    for source, values in svs:
        print '\t' + source + '\t' + '\t'.join(values)

"""
def get_term_counts(model_path):
    import cPickle as pickle

    count = 0
    tokens = 0
    fh = open(model_path + '.data','rb')
    try:
        while True:
            if count % 100000 == 0:
                print "Processed %i..." % count

            tfs = pickle.load(fh)
            tokens += sum(tfs.itervalues())
            count += 1
    except: pass

    return count, tokens

count, tokens = get_term_counts(wikilinks_mention_path)
print 'Wikilinks Mention: {:,d}\t{:,d}'.format(count, tokens)
count, tokens = get_term_counts(kopiwiki_article_path)
print 'Wikipedia Article: {:,d}\t{:,d}'.format(count, tokens)
count, tokens = get_term_counts(kopiwiki_mention_path)
print 'Wikipedia Mention: {:,d}\t{:,d}'.format(count, tokens)
"""

print "\n=Context model entity statistics..."
entity_sets = [(t,set(redirect_model.get(k, k) for k in m.iterkeys())) for t, m in context_models]
entity_sets_d = dict(entity_sets)
format_source_stats((s,['{:,d}'.format(len(es))]) for s, es in entity_sets)

for e in test_entities:
    if e not in entity_sets_d[KOPI_ARTICLE]:
        print 'MISSING: ' + e

print "\n=Macro (entity) coverage over: %s" % analysis_doc
entity_testb_intersection = [(s,es.intersection(test_entities)) for s, es in entity_sets]
format_source_stats((s,[
    '{:,d}'.format(len(es)),
    '{:,.2f}%'.format((100.0*len(es))/len(test_entities))
    ]) for s, es in entity_testb_intersection)

entity_testb_intersection_instance_count = [(s,
        sum(entity_counts[e] for e in es))
        for s, es in entity_testb_intersection]

print "\n=Micro (instance) coverage over %s:" % analysis_doc
format_source_stats((s,[
    '{:,d}'.format(c),
    '{:,.2f}%'.format((100.0*c)/len(links))
    ]) for s, c in entity_testb_intersection_instance_count)


def get_entity_term_statistics(model_path, entity_set):
    print 'Loading term statistics from: %s' % model_path
    entity_tfs = mmdict(model_path)
    return {e:0 if e not in entity_tfs else sum(entity_tfs[e].itervalues()) for e in entity_set} 

def dump_entity_perf_term_statistics(path, entity_set, entity_term_counts, entity_mention_counts):
    error_subset = [(m,[z for z in es if z[1] in entity_set]) for m, es in analysis_errors]
    link_subset = [l for l in links if l[1] in entity_set]
    print '\nError and Entity Coverage Analysis over {:,d} Entities'.format(len(entity_set))
    print "=Errors and P@1:"
    format_source_stats((s,[
        '{:,d}'.format(len(es)),
        '{:,.1f}%'.format((100.0*(len(link_subset) - len(es))) / len(link_subset))
        ]) for s, es in error_subset)

    errors_by_entity = [(m,Counter(e for _, e, _ in es)) for m, es in error_subset]

    # calculate the number of candidates per entity mention instance
    mention_ambiguity = defaultdict(list)
    for mention, gold_entity in links:
        mention_ambiguity[gold_entity].append(len(candidate_model.get(mention.lower(), [])))

    for m, ec_by_ent in errors_by_entity:
        if m in entity_term_counts:
            with open(path + '.' + m.replace(' ','_').lower() + '.tsv','w') as f:
                f.write('Entity\tInstances\tErrors\tContext Terms\tMentions\tMean Candidates Per Mention\tStd Candidates Per Mention\tPrecision\n')
                for e, count in entity_counts.iteritems():
                    if e in entity_set:
                        candidates_per_mention = numpy.mean(mention_ambiguity[e])
                        std_candidates_per_mention = numpy.std(mention_ambiguity[e])
                        error_count = ec_by_ent[e]
                        entity_p = (count - error_count) * 100.0 / float(count)
                        f.write('%s\t%i\t%i\t%i\t%i\t%.4f\t%.4f\t%.4f\n' % (e.encode('utf-8'), count, error_count, entity_term_counts[m][e], entity_mention_counts[m][e], candidates_per_mention, std_candidates_per_mention, entity_p))

joint_entity_set =                          \
    test_entities.intersection(            \
    entity_sets_d[WIKILINKS]).intersection( \
    entity_sets_d[KOPI_ARTICLE])        
"""
print '\nLoading entity mention counts...\n'
entity_mention_counts = {
    WIKILINKS: {e:0 for e in joint_entity_set},
    KOPI_ARTICLE: {e:1 for e in joint_entity_set}
}
for e, mentions in EntityMentionContexts.iter_entity_mentions_from_path(wikilinks_mentions_path):
    if e in joint_entity_set:
        entity_mention_counts[WIKILINKS][e] = len(mentions)

print '\nGetting entity term statistics...\n'
entity_term_counts = {m:get_entity_term_statistics(path, joint_entity_set) for m, path in context_model_paths.iteritems() if m != KOPI_MENTION}
dump_entity_perf_term_statistics('output/entity_perf_stats', joint_entity_set, entity_term_counts, entity_mention_counts)
"""
print 'Done..'

def errors_over_entity_set(entity_set):
    error_subset = [(m,[z for z in es if z[1] in entity_set]) for m, es in analysis_errors]
    link_subset = [l for l in links if l[1] in entity_set]
    print '\nError and Entity Coverage Analysis over {:,d} Entities'.format(len(entity_set))
    print "=Errors and P@1:"
    format_source_stats((s,[
        '{:,d}'.format(len(es)),
        '{:,.1f}%'.format((100.0*(len(link_subset) - len(es))) / len(link_subset))
        ]) for s, es in error_subset)

    print "\n=#Error Ents, #Uncovered Ents, #Uncovered Inst, %Ents Uncovered, %Errors Uncovered, P@1 on Covered"
    errors_by_entity = [(m,Counter(e for _, e, _ in es)) for m, es in error_subset]
    
    uncovered_errors_by_entity_d = dict(
        (s, 
            dict((e,es[e]) for e in es if e not in entity_sets_d[s])
        ) for s, es in errors_by_entity)

    format_source_stats((s,[
        '{:,d}'.format(len(es)),
        '{:,d}'.format(len(uncovered_errors_by_entity_d[s])),
        '{:,d}'.format(sum(uncovered_errors_by_entity_d[s].itervalues())),
        '{:,.1f}%'.format(100 * len(uncovered_errors_by_entity_d[s]) / float(len(es))),
        '{:,.1f}%'.format(100 * sum(uncovered_errors_by_entity_d[s].itervalues()) / float(sum(es.itervalues()))),
        '{:,.1f}%'.format(
            100.0*(1.0 - ((sum(es.itervalues())-sum(uncovered_errors_by_entity_d[s].itervalues()))
            /
            float((len(links) - sum(uncovered_errors_by_entity_d[s].itervalues()))))))
        ]) for s, es in errors_by_entity)


print '\n\n### Over the full set of entities in the test data:'
errors_over_entity_set(test_entities)

print '\n\n### Over test instances linking an entity covered by both wikilinks and wikipedia:'
errors_over_entity_set(joint_entity_set)

def invert_dict(original):
    inverted = defaultdict(set)
    for key, items in original.iteritems():
        for i in items:
            inverted[i].add(key)
    return inverted

def iter_value_counts(d):
    for v in d.itervalues():
        yield len(v)

def dict_set_stats(d, instance_name, key_name, value_name):
    inverted = invert_dict(d)
    total_uniq_keys = len(d)
    total_uniq_values = len(inverted)

    d_values = list(iter_value_counts(d))
    inv_values = list(iter_value_counts(inverted))

    print '\tTotal {:20s}:          {:12,d}'.format(instance_name, sum(iter_value_counts(d)))
    print '\tTotal {:20s}:          {:12,d}'.format(key_name, len(d))
    print '\tTotal {:20s}:          {:12,d}'.format(value_name, len(inverted))
    print '\tMean & Std.Dev {:s} per {:s}: {:12,.1f} {:12,.1f}'.format(key_name, value_name, numpy.mean(d_values), numpy.std(d_values))
    print '\tMean & Std.Dev {:s} per {:s}: {:12,.1f} {:12,.1f}'.format(value_name, key_name, numpy.mean(inv_values), numpy.std(inv_values))

print '\nLoading Name Count models...'
from webstrap.model.model import Name

wikipedia_name_model = Name(lower=True)
wikipedia_name_model.read(wikipedia_name_model_path)

wikilinks_name_model = Name(lower=True)
wikilinks_name_model.read(wikilinks_name_count_model_path)

print '\n=Wikipedia Name Model Statistics:'
dict_set_stats(wikipedia_name_model.d, 'Mentions', 'Name', 'Entity')
print '\n=Wikilinks Name Model Statistics:'
dict_set_stats(wikilinks_name_model.d, 'Mentions', 'Name', 'Entity')



print '\nLoading Link statistics:'
from webstrap.model.model import EntityOccurrence
wikipedia_occurrence_model = EntityOccurrence.read(wikipedia_occurrence_model_path)
wikilinks_occurrence_model = EntityOccurrence.read(wikilinks_occurrence_model_path)

print '\n=Wikipedia Link statistics:'
dict_set_stats(wikipedia_occurrence_model.d, 'Occurrences', 'Entity', 'Page')

print '\n=Wikilinks Link statistics:'
dict_set_stats(wikilinks_occurrence_model.d, 'Occurrences', 'Entity', 'Page')




print '\n'
if len(sys.argv) >= 2 and sys.argv[1] == 'interact':
    code.interact(local=locals())


#print "\nComputing mention context statistics..."
#context_entity_token_counts = [(s,{entity:sum(counts.itervalues()) for entity, counts in ctx.iteritems()}) for s, ctx in context_models]

#print "\nTokens per entity:"
#format_source_stats((s,[
#    '{:,.1f}'.format(sum(ets.itervalues()) / float(len(ets))),
#    ]) for s, ets in context_entity_token_counts)
