#!/bin/bash
set -e

WSROOT=${WSROOT:=$HOME/bobwilson}


DATASET_ROOT=tac/2014
DOCSET_NAME=test

DATASET_PATH=$WSROOT/data/$DATASET_ROOT
DOC_LIST_PATH=$DATASET_PATH/$DOCSET_NAME\_doclist.txt
#QUERIES_PATH=$DATASET_PATH/$DOCSET_NAME\_queries.xml

#DR_DOC_PATH=/data0/glen/e12_take2_splits
DR_DOC_PATH=/data0/glen/e45_dr_splits

OUTPUT_PATH=$DATASET_PATH/docs

$WSROOT/webstrap/utility/tac_dr_fetch_c.sh $DOC_LIST_PATH $OUTPUT_PATH $DR_DOC_PATH
