MSG=$1
LAT=-33.874
LNG=151.203

HOST=localhost
PORT=1447

DATA="{
    \"docs\":[{
        \"id\": 0,
        \"content\": \"Test document mentioning some guy like John Howard the Australian politician.\"
    }]
}"

curl -X POST -H "Content-Type: application/json" -d "$DATA" http://$HOST:$PORT/ | python -m json.tool
