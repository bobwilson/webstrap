#!/bin/bash
set -e

WSROOT=${WSROOT:=$HOME/bobwilson}

DATA_HOST_NAME=schwa11
MODEL_PATH=/n/$DATA_HOST_NAME/data0/linking/models
DATA_ROOT=/n/$DATA_HOST_NAME/data0/linking/data

DATA_PATH=$DATA_ROOT/conll
#DATA_PATH=$DATA_ROOT/tac/2014

DOCS=()
DOCS+=(train)
#DOCS+=(test)
#DOCS+=(train_h1)
#DOCS+=(train_h2)
#DOCS+=(test09train10)
#DOCS+=(testa)
#DOCS+=(testb)

TRAIN_BASE_DOC_NAME=train

for DOC_BASE_NAME in "${DOCS[@]}"; do

DOC_MODEL_PATH=$DATA_PATH/prepared/$DOC_BASE_NAME.doc.model
FEATURE_BASE_PATH=$DATA_PATH/features

FEATURES=()
#FEATURES+=(FreebaseScore)
#FEATURES+=(EntityProbability)
#FEATURES+=(NameProbability)
#FEATURES+=(NameGivenEntityProbability)
#FEATURES+=(BoWMentionContext)
#FEATURES+=(CorefNameProbability)
#FEATURES+=(AvgDBoWMentionContext)
#FEATURES+=(LLMDBoWMentionContext)
#FEATURES+=(DMaxBoWMentionContext)

ENTITY_CTX_MODELS=()
#ENTITY_CTX_MODELS+=(wikilinks)
#ENTITY_CTX_MODELS+=(kopiwiki_mention)
#ENTITY_CTX_MODELS+=(kopiwiki_article)

NGE_PROB_MODELS=()
#NGE_PROB_MODELS+=(wikilinks)

NAME_MODELS=()
#NAME_MODELS+=(wikipedia)
#NAME_MODELS+=(wikilinks)

ENTITY_MODELS=()
#ENTITY_MODELS+=(wikipedia)
#ENTITY_MODELS+=(wikilinks)

for FEATURE in "${FEATURES[@]}"; do

if [[ $FEATURE == *BoW* ]]; then
    ITER_ENTITY_CTX_MODELS=("${ENTITY_CTX_MODELS[@]}")
else
    ITER_ENTITY_CTX_MODELS=('')
fi

if [[ $FEATURE == NameGivenEntityProbability ]]; then
    ITER_NGE_PROB_MODELS=("${NGE_PROB_MODELS[@]}")
else
    ITER_NGE_PROB_MODELS=('')
fi

if [[ $FEATURE == *NameProbability ]]; then
    ITER_NAME_MODELS=("${NAME_MODELS[@]}")
else
    ITER_NAME_MODELS=('')
fi

if [[ $FEATURE == EntityProbability ]]; then
    ITER_ENTITY_MODELS=("${ENTITY_MODELS[@]}")
else
    ITER_ENTITY_MODELS=('')
fi

for ENTITY_CTX_MODEL in "${ITER_ENTITY_CTX_MODELS[@]}"; do
for NGE_PROB_MODEL in "${ITER_NGE_PROB_MODELS[@]}"; do
for NAME_MODEL in "${ITER_NAME_MODELS[@]}"; do
for ENTITY_MODEL in "${ITER_ENTITY_MODELS[@]}"; do

    QUERY_CTX_WINDOW=full
    IDF_MODEL=wikilinks.entity_context.w_none
    ENTITY_CTX_WINDOW=none
    WORDVEC_MODEL=googlenews300

    MODEL_DESC=$FEATURE
    MODEL_PARAMS=()

    if [[ $FEATURE == EntityProbability ]]; then
        ENTITY_MODEL_PATH=$MODEL_PATH/$ENTITY_MODEL.entity.model
        MODEL_PARAMS+=($ENTITY_MODEL_PATH)
        MODEL_DESC=$MODEL_DESC.$ENTITY_MODEL
    fi

    if [[ $FEATURE == *NameProbability ]]; then
        NAME_MODEL_PATH=$MODEL_PATH/$NAME_MODEL.name.model
        MODEL_PARAMS+=($NAME_MODEL_PATH)
        MODEL_DESC=$MODEL_DESC.$NAME_MODEL
    fi

    if [[ $FEATURE == NameGivenEntityProbability ]]; then
        NGE_PROB_MODEL_PATH=$MODEL_PATH/$NGE_PROB_MODEL.probability_name_entity.model
        MODEL_PARAMS+=($NGE_PROB_MODEL_PATH)
        MODEL_DESC=$MODEL_DESC.$NGE_PROB_MODEL
    fi

    if [[ $FEATURE == *BoW* ]]; then
        ENTITY_IDF_MODEL_PATH=$MODEL_PATH/$ENTITY_CTX_MODEL.entity_context.w_$ENTITY_CTX_WINDOW.idf.model
        #ENTITY_IDF_MODEL_PATH=$MODEL_PATH/$IDF_MODEL\.idf.model
        ENTITY_TF_MODEL_PATH=$MODEL_PATH/$ENTITY_CTX_MODEL.entity_context.w_$ENTITY_CTX_WINDOW.tf.model
        
        MODEL_PARAMS+=(
            full
            $ENTITY_IDF_MODEL_PATH
            $ENTITY_TF_MODEL_PATH)

        MODEL_DESC=$MODEL_DESC.$ENTITY_CTX_MODEL
        
        if [[ $FEATURE == *DBoW* ]]; then
            WORDVEC_MODEL_PATH=$MODEL_PATH/$WORDVEC_MODEL.wordvector.model
            MODEL_PARAMS+=($WORDVEC_MODEL_PATH)
        fi
    fi
    MODEL_DESC=${MODEL_DESC//dbpedia/kopiwiki_article}
    FEATURE_MODEL_PATH=$FEATURE_BASE_PATH/$DOC_BASE_NAME.$MODEL_DESC.feature.model
    
    if [[ $DOC_BASE_NAME == $TRAIN_BASE_DOC_NAME ]]; then
        STANDARDISATION_PARAM=
    else
        STANDARDISATION_MODEL_PATH=$FEATURE_BASE_PATH/$TRAIN_BASE_DOC_NAME.$MODEL_DESC.feature.model.standardisation.model
        STANDARDISATION_PARAM="--standardisation_model_path $STANDARDISATION_MODEL_PATH"
    fi

    RUN_PARAMS=(
        $DOC_MODEL_PATH
        $STANDARDISATION_PARAM
        $FEATURE_MODEL_PATH
        $FEATURE)

    for p in "${MODEL_PARAMS[@]}"; do
        RUN_PARAMS+=($p)
    done

    RUN_PARAMS=$(printf " %s" "${RUN_PARAMS[@]}")

    echo "Extracting feature: $MODEL_DESC"
    echo "Params: $RUN_PARAMS"
    $WSROOT/ws extract-feature $RUN_PARAMS

done # entity model
done # entity given name model
done # name given entity model
done # context model iteration
done # feature type
done # extraction doc iteration
