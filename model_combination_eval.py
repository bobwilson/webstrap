import glob

import cPickle as pickle
import marshal
import numpy
import os
import copy
import math
import sys

from webstrap.process.train import Train
from webstrap.link.supervised import SupervisedRanker
from webstrap.link.unsupervised import DiscriminativeLinker
from webstrap.util import parmapper
from itertools import chain,combinations,product
from time import time
import datetime

import logging

if len(sys.argv) < 2:
    print 'Provide a run name.'
    exit()

run_time = time()
run_name = sys.argv[1]

## Logging Configuration
logFormat = '%(asctime)s|%(levelname)s|%(module)s|%(message)s'
logging.basicConfig(format=logFormat)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

# Eval Configuration
dataset_root = 'conll'
train_doc_name = 'train'
test_doc_name = 'testa'

data_host_name = 'schwa11'
data_root = '/n/%s/data0/linking' % data_host_name

data_path = '%s/data/%s' % (data_root, dataset_root)
output_path = 'output/%s' % (dataset_root)

train_doc_path = '%s/prepared/%s.doc.model' % (data_path, train_doc_name)
test_doc_path = '%s/prepared/%s.doc.model' % (data_path, test_doc_name)

train_features_path = '%s/features/%s.*.feature.model' % (data_path, train_doc_name)
test_features_path = '%s/features/%s.*.feature.model' % (data_path, test_doc_name)

redirect_map_path = '%s/models/dbpedia.redirect.model' % data_root

def iter_powerset(items): # powerset - w/o empty set
    return chain(*(combinations(items, sz) for sz in xrange(1, len(items)+1)))

def load_features(glob_path):
    features = {}
    for path in glob.glob(glob_path):
        feature_name = '.'.join(os.path.basename(path).split('.')[1:-2])
        features[feature_name] = pickle.load(open(path, 'rb'))
    return features

def load_docs(doc_model_path):
    docs = []
    with open(doc_model_path, 'rb') as fh:
        while True:
            try:
                doc = pickle.load(fh)
                
                # todo: duplicate candidate bug
                for m in doc.mentions:
                    m.candidates = list(set(m.candidates))

                docs.append(doc)
            except EOFError: break
    return docs

print 'Loading redirect map...'
redirect_map = marshal.load(open(redirect_map_path, 'rb'))

import random
random.seed(42)

print 'Loading document models...'
train_doc = load_docs(train_doc_path)
random.shuffle(train_doc)

test_doc = load_docs(test_doc_path)

print 'Loading features...'
train_features = load_features(train_features_path)
test_features = load_features(test_features_path)

num_features = len(train_features)
num_combinations = (2 ** num_features) - 1


def correct_at(candidates, gold, n):
    gold = redirect_map.get(gold, gold)
    for _, candidate in candidates:
        candidate = redirect_map.get(candidate, candidate)
        if n > 0:
            n -= 1
            if candidate == gold:
                return True
        else: return False

def evaluate_feature_configuration(configuration):
    train_feature_set = [train_features[f] for f in train_features.iterkeys()]
    test_feature_set = [test_features[f] for f in train_features.iterkeys()]

    model = Train.train(train_doc, train_feature_set, **configuration)
    if 'kernel' in configuration:
        ranker = SupervisedRanker(model, test_feature_set, kernel=configuration['kernel'])
    else:
        ranker = SupervisedRanker(model, test_feature_set)
    
    correct = 0
    total = 0
    for d in test_doc:
        linked_d = ranker(copy.deepcopy(d))
        correct += sum(1 for m in linked_d.mentions if correct_at(m.candidates, d.gold_links_by_position[(m.begin, m.end)], 1))
        total += len(linked_d.mentions)

    return configuration, float(correct)*100.0 / total

experiments = {
    #'penalty':              ['l2'],
    #'loss':                 ['l1'],
    #'C':                    [math.pow(10, i) for i in numpy.arange(-7.5,8,0.5) ],
    #'kernel':               ['poly'],
    #'instance_selection':   ['mag','random'],
    #'instance_limit':       [1, 10],
    'train_instance_limit':  [i for i in range(18500,18600,100)]
}

experiment_instances = list(product(*experiments.values()))

#experiments = [math.pow(10, i) for i in numpy.arange(-8.5,9,0.5) ] #range(10, 18600, 100) # 18512 total in conll train
num_combinations = len(experiment_instances)
print 'Evaluating %i combinations for %i features...' % (num_combinations, num_features)

completed = 0
def iter_feature_sets():
    for i, config in enumerate(experiment_instances):
        log.info('Evaluating config: %i / %i' % (i+1, num_combinations))

        mapped_config = dict(zip(experiments.keys(), config))

        if  (mapped_config.get('penalty','l2') == 'l1' and mapped_config.get('loss','l2') == 'l1') or \
            (mapped_config.get('instance_limit','10') == None and mapped_config.get('instance_selection','random') != 'random'):
            log.info('Skipping config: %i / %i' % (i+1, num_combinations))
            continue

        log.info('Evaluating config: %i / %i' % (i+1, num_combinations))
        yield mapped_config # reassemble into dict

start_time = time()
results = []
with parmapper(evaluate_feature_configuration, 31, recycle_interval=1) as pm:
    log.debug("Starting parallel model evaluation...")
    for i, (configuration, precision) in pm.consume(iter_feature_sets()):
        completed += 1
        rate_per_min = completed * 60 / (time() - start_time)
        log.info('%i / %i Complete. Eta: %.2f mins' % (completed, num_combinations, (num_combinations - completed) / rate_per_min))
        results.append((configuration, precision))

with open('%s/training_%s.tsv' % (output_path, run_name), 'wb') as f:
    f.write('P@1\t%s\n' % '\t'.join(experiments.keys()))
    for configuration, p in results:
        values = [configuration[k] for k in experiments.iterkeys()]
        fmt_values = ['%f' % v if isinstance(v,float) else str(v) for v in values]
        f.write('%.6f\t%s\n' % (p, '\t'.join(fmt_values)))
"""
def evaluate_feature_configuration(feature_set):
    train_feature_set = [train_features[f] for f in feature_set]
    test_feature_set = [test_features[f] for f in feature_set]

    model = Train.train(train_doc, train_feature_set)
    ranker = SupervisedRanker(model, test_feature_set)
    
    correct = 0
    total = 0
    for d in test_doc:
        linked_d = ranker(copy.deepcopy(d))
        correct += sum(1 for m in linked_d.mentions if correct_at(m.candidates, d.gold_links_by_position[(m.begin, m.end)], 1))
        total += len(linked_d.mentions)

    return feature_set, float(correct)*100.0 / total

def iter_feature_sets():
    for i, feature_set_iter in enumerate(iter_powerset(train_features.keys())):

        log.info('Evaluating config: %i / %i' % (i+1, num_combinations))
        feature_set_list = list(feature_set_iter)
        #if len(feature_set_list) >= len(train_features.keys()) - 1:
        yield feature_set_list

start_time = time()
completed = 0
results = []
with parmapper(evaluate_feature_configuration, 31, recycle_interval=1) as pm:
    log.debug("Starting parallel model evaluation...")
    log.debug("Evaluating combinations of:")
    for f in train_features.iterkeys():
        log.debug("\t%s" % f)

    for i, (feature_set, precision) in pm.consume(iter_feature_sets()):
        completed += 1
        rate_per_min = completed * 60 / (time() - start_time)
        log.info('%i / %i Complete. Eta: %.2f mins' % (completed, num_combinations, (num_combinations - completed) / rate_per_min))
        results.append((set(feature_set), precision))

with open(output_path + '/combinations_%s.tsv' % (run_name), 'wb') as f:
    features = train_features.keys()
    f.write('P@1\t' + '\t'.join(features) + '\n')
    for fs, p in results:
        p_fmt = '%.6f\t' % p
        f.write(p_fmt + '\t'.join('1' if f in fs else '0' for f in features) + '\n')
"""

#import code
#code.interact(local=locals())
