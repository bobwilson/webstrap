#print 'WIP!'
#exit()

from collections import defaultdict
import marshal
import cPickle as pickle
import sys
import os

data_host_name = 'schwa11'
data_set_name = 'tac/2014'
root_path = '/n/%s/data0/linking' % data_host_name
root_data_path = '%s/data/%s' % (root_path, data_set_name)
doc_model_path =  '%s/prepared' % root_data_path
classifiers_path = '%s/features/classifiers' % root_data_path

if len(sys.argv) != 3:
    print 'Run as:\n\t%s\tRESULT_TSV_PATH\tGOLD_TSV_PATH' % sys.argv[0]
    exit()

system_annots_path = sys.argv[1]
gold_annots_path = sys.argv[2]
results_name_parts = os.path.basename(system_annots_path).split('.')
doc_model_name = results_name_parts[0]
system_hash = results_name_parts[-2]
system_id = doc_model_name + '.' + system_hash

print "System ID: %s" % system_id
classifier_score_model_path = '%s/%s.evaluation.classifier.feature.model' % (classifiers_path, system_id)
classifier_score_standardisation_model_path = '%s/%s.evaluation.classifier.feature.model.standardisation.model' % (classifiers_path, system_id)

print 'Loading evaluation scores...'
classifier_score_model = pickle.load(open(classifier_score_model_path, 'rb'))
mean, std = pickle.load(open(classifier_score_standardisation_model_path, 'rb'))

# mapping back to raw scores
for doc_id in classifier_score_model.iterkeys():
    for mention_key in classifier_score_model[doc_id].iterkeys():
        for candidate, value in classifier_score_model[doc_id][mention_key].iteritems():
            classifier_score_model[doc_id][mention_key][candidate] = value * std + mean

print 'Loading evaluation docs...'
eval_docs = []
eval_doc_model_path = '%s/%s.doc.model' % (doc_model_path, doc_model_name)
with open(eval_doc_model_path, 'rb') as f:
    while True:
        try:
            eval_docs.append(pickle.load(f))
        except EOFError: break

print 'Performing evaluation...'
tac_to_wk_entity_map_path = '%s/data/tac/2010/kb/tackbid2wptitle.tab' % root_path
redirect_model_path = '%s/models/wikipedia.redirect.model' % root_path

def load_tac_map(path):
    with open(path, 'r') as f:
        return dict(tuple(l.decode('utf-8').strip().split('\t')[:2]) for l in f if l.strip() != '')

redirect_model = marshal.load(open(redirect_model_path,'rb'))
system_annots = load_tac_map(system_annots_path)


tac_to_wk_entity_map = load_tac_map(tac_to_wk_entity_map_path)
tac_to_wk_entity_map = {k:redirect_model.get(v,v) for k,v in tac_to_wk_entity_map.iteritems()}
wk_to_tac_entity_map = {v:k for k,v in tac_to_wk_entity_map.iteritems()}
tac_wk_entities = set(tac_to_wk_entity_map.itervalues())

"""
# NIL Threshold estimation
all_gold_annots = load_tac_map(gold_annots_path)
gold_annots = {k:tac_to_wk_entity_map[v] for k,v in all_gold_annots.iteritems() if not v.startswith('NIL')}
gold_nil_queries = set(k for k,v in all_gold_annots.iteritems() if v.startswith('NIL'))
gold_non_nil_queries = set(k for k,v in all_gold_annots.iteritems() if not v.startswith('NIL'))
all_queries = set(all_gold_annots.iterkeys())

# NIL Clustering
for threshold in xrange(-100, 16, 1):
    threshold = threshold * 0.25
    system_nil_queries = set()
    for d in eval_docs:
        for m in d.mentions:
            is_nil = True
            if m.candidates and system_annots[m.query_id] in tac_wk_entities:
                if max(classifier_score_model[d.id][(m.begin, m.end)].itervalues()) > threshold:
                    is_nil = False
            if is_nil:
                system_nil_queries.add(m.query_id)

    tp = len(gold_nil_queries & system_nil_queries)
    tn = len(gold_non_nil_queries & (all_queries - system_nil_queries))
    fp = len(system_nil_queries - gold_nil_queries)
    fn = len(gold_nil_queries - system_nil_queries)
    print '%.1f\t%.3f\t%.3f\t%.3f\t%.3f' % (threshold, float(tp + tn) / len(all_queries), float(tp) / (tp + fp), float(tp) / (tp + fn), 2.0*tp / (2.0*tp + fp + fn))
"""

threshold = 1 # 2.5
system_nil_queries = set()
system_nil_entity_clusters = defaultdict(set)
system_nil_name_clusters = defaultdict(set)
for d in eval_docs:
    for m in d.mentions:
        is_nil = True
        if m.candidates and system_annots[m.query_id] in tac_wk_entities:
            if max(classifier_score_model[d.id][(m.begin, m.end)].itervalues()) > threshold:
                is_nil = False
        if is_nil:
            system_nil_queries.add(m.query_id)
            if m.query_id in system_annots and system_annots[m.query_id] in tac_wk_entities:
                # todo: we might want a different threshold here
                system_nil_entity_clusters[system_annots[m.query_id]].add(m.query_id)
            else:
                longest_mention_name = sorted([z.text.lower() for z in m.chain.mentions], key=len, reverse=True)[0]
                system_nil_name_clusters[longest_mention_name].add(m.query_id)

qid_to_nil_cluster = {}
for entity, qids in system_nil_entity_clusters.iteritems():
    for qid in qids:
        qid_to_nil_cluster[qid] = 'NIL_WK_' + entity
for name, qids in system_nil_name_clusters.iteritems():
    for qid in qids:
        qid_to_nil_cluster[qid] = 'NIL_NM_' + name.replace(' ', '_')
results = {qid:wk_to_tac_entity_map[v] for qid, v in system_annots.iteritems() if qid not in qid_to_nil_cluster}
for qid, nil_id in qid_to_nil_cluster.iteritems():
    results[qid] = nil_id

with open('%s.tac_eval.tab' % system_annots_path, 'w') as f:
    for qid, eid in results.iteritems():
        f.write('\t'.join([qid, eid, '1.0']).encode('utf-8') + '\n')

exit()

#import code
#code.interact(local=locals())

"""
nil_annots = {k:v[3:] for k,v in all_gold_annots.iteritems() if v.startswith('NIL')}
nil_clusters = defaultdict(set)
for qid, nid in nil_annots.iteritems():
    nil_clusters[nid].add(qid)

valid_nil_count = 0
nil_count = 0
for qid, entity in system_annots.iteritems():
    if entity not in tac_wk_entities:
        nil_count += 1
        if qid in nil_annots:
            valid_nil_count += 1

print ''
print 'System annotations:              %i' % len(system_annots)
print 'System linked NIL count:         %i' % nil_count
print 'System valid linked NIL count:   %i' % valid_nil_count
print ''
"""

correct_annots = {k:v for k,v in system_annots.iteritems() if k in gold_annots and v == gold_annots[k]}
incorrect_annots = {k:v for k,v in system_annots.iteritems() if k in gold_annots and v != gold_annots[k]}
missing_annots = {k:v for k,v in gold_annots.iteritems() if k not in system_annots}

tp = len(correct_annots)
fp = len(incorrect_annots)
tn = len(missing_annots)

print 'Correct:   %i' % tp
print 'Incorrect: %i' % fp
print 'Missing:   %i' % tn
print ''
print 'Acc = %.3f' % (float(tp) / len(gold_annots))
print 'P@1 = %i / %i = %.3f' % (tp, tp + fp, (float(tp) / (tp + fp)))
print 'R   = %i / %i = %.3f' % (tp + fp, tp+fp+tn, (float(tp+fp)/(tp+fp+tn)))
print ''

mistakes = {k:(v,gold_annots[k]) for k,v in incorrect_annots.iteritems()}

#import code
#code.interact(local=locals())
